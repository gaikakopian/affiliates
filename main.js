$(document).ready(function(){
	$(".refresh_container").click(function(e) {
		$(".refresh-icon").toggleClass("refresh_icon_active");

		/*The text of the paragraphs changes after 1 second*/
		setTimeout(function(){
			/*btc_send_p TOGGLE*/
			$(".btc_send_p").toggleClass("btc_send_p_toggle");
			$(".btc_send_container").fadeOut( 200, function() {
    // Animation complete
    if($(".btc_send_p").hasClass("btc_send_p_toggle")) {
    	$(".btc_send_p").text("Рубли отправляете");
    }
    else {
    	$(".btc_send_p").text("BTC отправляете");
    }
});
			$(".btc_send_container").fadeIn( 200, function() {
    // Animation complete
});


			/*btc_get_p TOGGLE*/
			$(".btc_get_p").toggleClass("btc_get_p_toggle");
			$(".btc_get_container").fadeOut( 200, function() {
    // Animation complete
    if($(".btc_get_p").hasClass("btc_get_p_toggle")) {
    	$(".btc_get_p").text("BTC получаете");
    }
    else {
    	$(".btc_get_p").text("Рубли получаете");
    }	
});
			$(".btc_get_container").fadeIn( 200, function() {
    // Animation complete
});



			$(".qiwi_wallet_p").toggleClass("qiwi_wallet_p_toggle");
			$(".qiwi_wallet_container").fadeOut( 200, function() {
	    // Animation complete
	    if($(".qiwi_wallet_p").hasClass("qiwi_wallet_p_toggle")) {
	    	$(".qiwi_wallet_p").text("Адрес получателя");
	    }
	    else {
	    	$(".qiwi_wallet_p").text("QIWI кошелек получателя");
	    }	
	});
			$(".qiwi_wallet_container").fadeIn( 200, function() {
	    // Animation complete
	});


			$(".Mainpage_sell_btn").toggleClass("Mainpage_sell_btn_toggle");
			$(".Mainpage_sell_btn_container ").fadeOut( 200, function() {
	    // Animation complete
	    if($(".Mainpage_sell_btn").hasClass("Mainpage_sell_btn_toggle")) {
	    	$(".Mainpage_sell_btn").text("Купить");
	    }
	    else {
	    	$(".Mainpage_sell_btn").text("Продать");
	    }
	});

			$(".Mainpage_sell_btn_container ").fadeIn( 200, function() {
	    // Animation complete

	});


		},1000);
	});

	/*Filter Click Menu*/
	$(document).on("click",".custom-back-img",function(e) {
		$(".filter-click").toggleClass("custom-back-img-active");
		if($(".filter-click").hasClass("custom-back-img-active")) {
			$(".filter-field-Show").css("display", "block");
			$(".custom-back-img a").removeClass("custom-back-img-default-arrow");
		}
		else {
			$(".filter-field-Show").css("display", "none");
			$(".custom-back-img a").addClass("custom-back-img-default-arrow");
		}		
	});

/*Search Input Focus*/

	    $(document).on('focus' ,'.search-input',function(){
            $('.search_img_default').hide();
            $('.search_img_change').show();
        });

        $(document).on('focusout' ,'.search-input',function(){
        	$('.search_img_change').hide();
        	$('.search_img_default').show();
      });

/*Search Input Focus*/


/*Delete Input Hover*/
        $(document).on('mouseover' ,'.delete-icon',function(){
            $(this).hide();
            $(this).siblings().show();
        });	

        $(document).on('mouseout' ,'.delete-icon-black',function(){
        	$(this).hide();
            $(this).siblings().show();         
        });
/*Delete Input Hover*/


/*Pause Input Hover*/
	    $(document).on('mouseover' ,'.pause-icon',function(){
            $(this).hide();
            $(this).siblings().show(); 
        });	

        $(document).on('mouseout' ,'.pause-icon-black',function(){
            $(this).hide();
            $(this).siblings().show();
        });
/*Pause Input Hover*/


	/*Jquery End*/
});

/*Timer*/

function getTimeRemaining(endtime) {
	var t = Date.parse(endtime) - Date.parse(new Date());
	var seconds = Math.floor((t / 1000) % 60);
	var minutes = Math.floor((t / 1000 / 60) % 60);
	var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
	var days = Math.floor(t / (1000 * 60 * 60 * 24));
	return {
		'total': t,
		'hours': hours,
		'minutes': minutes,
		'seconds': seconds
	};
}

function initializeClock(id, endtime) {
	var clock = document.getElementById(id);

	function updateClock() {
		var t = getTimeRemaining(endtime);

		var hoursSpan = ('0' + t.hours).slice(-2);
		var minutesSpan = ('0' + t.minutes).slice(-2);
		var secondsSpan = ('0' + t.seconds).slice(-2);
		document.getElementById("chronometer_timer").innerHTML = hoursSpan + ":" + minutesSpan + ":" + secondsSpan;
		if (t.total <= 0) {
			clearInterval(timeinterval);
		}
	}

	updateClock();
	var timeinterval = setInterval(updateClock, 1000);
}

var deadline = new Date(Date.parse(new Date()) + 2 * 60 * 60 * 1000);
initializeClock('clockdiv', deadline);

/*Timer*/

