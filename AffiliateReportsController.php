<?php

    namespace App\Http\Controllers\Affiliate;

    use App\Helpers\CustomPaginationLinks;
    use App\Helpers\ExportHelper;
    use App\Helpers\GeneralHelper;
    use App\Helpers\Permissions;
    use App\Helpers\ReportsHelper;
    use App\Helpers\TimeHelper;
    use App\Http\Controllers\Controller;
    use App\Models\AffiliateRequest;
    use App\Models\Commission;
    use App\Models\Product;
    use App\Models\Purchase;
    use App\Models\Visit;
    use App\User;
    use Auth;
    use Carbon\Carbon;
    use DB;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Input;
    use Illuminate\Http\Request;
    use View;

    class AffiliateReportsController extends Controller
    {


        private $_data = array();

        /**
         * Construct
         *
         */
        public function __construct(Request $request)
        {
            $this->request = $request;
            parent::__construct();
            $this->_data['nav'] = "reports";
        }

        /**
         * Not used anymore!
         *
         * Campaigns Report
         *
         * @return \Illuminate\View\View|\Wpb\String_Blade_Compiler\StringView
         */
        public function getCampaignsReports()
        {
            $this->_data = $this->getCampaignsReportList();
            $this->_data['campaigns_reports'] = $this->_data['campaigns_reports']->paginate(10);

            //Array for sorted campaigns list
            $sorted_campaigns_list = [];
            //Generate new array
            foreach ($this->_data['campaigns'] as $key => $value) {
                $sorted_campaigns_list[$value->name] = $value;
            }

            ksort($sorted_campaigns_list, SORT_NATURAL | SORT_FLAG_CASE);

            //Array for sorted vendors list
            $sorted_vendors_list = [];
            //Generate new array
            foreach ($this->_data['vendors'] as $key => $value) {
                $sorted_vendors_list[$value->name] = $value;
            }

            ksort($sorted_vendors_list, SORT_NATURAL | SORT_FLAG_CASE);

            $this->_data['campaigns'] = $sorted_campaigns_list;
            $this->_data['vendors'] = $sorted_vendors_list;
            $this->_data['query'] = $this->processQueryData()['query'];

            return View::make('affiliate.report.campaigns.campaign-report', $this->_data);
        }

        /**
         * @param null   $user_id
         * @param string $type
         *
         * @return mixed
         */
        public function getCampaignsReportList($user_id = null, $type = 'affiliate')
        {
            if (!$user_id)
                $user_id = Auth::user()->id;

            $filterMap = [
                'vendor_id'   => 'users.id',
                'campaign_id' => 'products.id',
            ];

            $campaigns_reports = DB::table('plans')
                ->join('product_permissions', 'product_permissions.product_id', '=', 'plans.product_id')
                ->join('products', 'products.id', '=', 'plans.product_id')
                ->join('users', 'users.id', '=', 'products.user_id')
                ->leftJoin(DB::raw('(SELECT product_id, COUNT(product_id) as visits FROM visit_products GROUP BY product_id) as v'), 'v.product_id', '=', 'plans.id')
                ->leftJoin(DB::raw('(SELECT plan_id, COUNT(plan_id) as sales, SUM(amount) AS totalSales FROM purchases GROUP BY plan_id) as p'), 'p.plan_id', '=', 'plans.id')
                ->leftJoin(DB::raw('(SELECT plan_id, SUM(amount) AS totalCommissions FROM commissions GROUP BY plan_id) as c'), 'c.plan_id', '=', 'plans.id')
                ->where('product_permissions.user_id', [$user_id])
                ->whereNotNull('plans.id')
                ->whereIn('product_permissions.promote', ['2', '1'])
                ->distinct()
                ->select('plans.id',
                    'products.name as campaign',
                    DB::raw('CONCAT(users.first_name, " ", users.last_name) AS vendor'),
                    'plans.name as product',
                    'sales',
                    'totalSales',
                    'visits',
                    'totalCommissions',
                    DB::raw('ROUND(sales * 100 / visits) as conversionRate'),
                    DB::raw('ROUND(totalSales * 100 / visits) as epc')
                );

            $search = Input::get('q');
            $filters = $this->getFilters($_GET);
            $main_query = clone $campaigns_reports;

            if ($search) {
                $campaigns_reports = $campaigns_reports->where(function ($query) use ($search) {
                    $query->where('users.first_name', 'like', '%' . $search . '%')
                        ->orWhere('users.last_name', 'like', '%' . $search . '%')
                        ->orWhere('products.name', 'like', '%' . $search . '%')
                        ->orWhere('plans.name', 'like', '%' . $search . '%');
                });
            }

            if (!empty($filters)) {
                foreach ($filters as $index => $filter) {
                    $campaigns_reports = $campaigns_reports->where($filterMap[$index], $filter);
                }
            }

            $data['vendors'] = $main_query->select('users.id',
                DB::raw('CONCAT(users.first_name, " ", users.last_name) AS name'))
                ->distinct('users.id')
                ->get();

            $data['campaigns'] = $main_query->select('products.id', 'products.name')
                ->distinct('products.id')
                ->get();

            $data['search'] = $search;
            $data['campaigns_reports'] = $campaigns_reports;

            return $data;
        }

        /**
         * Not used anymore!
         *
         * Export Campaigns Reports
         */
        public function exportCampaignsReports()
        {
            $reports = $this->getCampaignsReportList();
            $report_lists = $reports['campaigns_reports']->get();

            $campaigns_reports = [
                [
                    'Vendor',
                    'Ccampaign',
//                'Product',
                    'Clicks',
                    'Visitors',
                    'Sales',
                    'Conversions',
                    'EPC',
                    'EPS',
                    'Total Sales',
                    'Total Commissions',
                ],
            ];
            foreach ($report_lists as $reports) {
                $campaigns_reports[] = [
                    $reports->vendor,
                    $reports->campaign,
//                $reports->product,
                    0,
                    $reports->visits,
                    $reports->sales,
                    $reports->conversionRate,
                    $reports->epc,
                    0,
                    $reports->totalSales,
                    $reports->totalCommissions,
                ];
            }

            ExportHelper::exportCSVFromList('Affiliate Campaigns Reports\' List', 'Affiliate Campaigns Reports', $campaigns_reports);
        }

        /**
         * Sales Report
         *
         * @return \Illuminate\View\View|\Wpb\String_Blade_Compiler\StringView
         */
        public function getSalesReports()
        {
            $affiliate_id = auth()->user()->id;
            $searchIds = Permissions::getAllowedProducts('promote');

            $range = GeneralHelper::getDateRange();

            $data = $this->processQueryData();
            $data['startDate'] = Carbon::parse($range['startDate'])->format('m/d/Y');
            $data['endDate'] = Carbon::parse($range['endDate'])->format('m/d/Y');
            $data['filters'] = array_merge($data['filters'], $range);

            // Get all affiliates from the Visits (traffic) table
            $data['reports'] = ReportsHelper::getSalesReports($searchIds, $data, 10, $affiliate_id);

            $data['campaigns'] = Product::select(['id', 'name'])
                ->whereIn('id', $searchIds)
                ->get();

            return view('affiliate.report.sales.sales-report2', $data);

            /*$range = GeneralHelper::getDateRange();
            $request = Request::all();

            $f_data = $this->processQueryData();

            if (isset($request['campaign_id'])) {
                $ids = explode(',', $request['campaign_id']);

                $searchIds = array_intersect($searchIds, $ids);
            }

            $data = Plan::select([
                DB::raw('CONCAT(users.first_name, " ", users.last_name) as vendor_name'),
                'products.id as product_id', 'products.name as product_name',
                'plans.id as plan_id1',
                'plans.name as plan_name',
                DB::raw('sum(ifnull(purchases.amount, 0)) as total_amount'),
                DB::raw('count(purchases.id) as sales'),
//				DB::raw('count(visit_products.id) as visits'),
                DB::raw('0 as visits'),
                DB::raw('sum(ifnull(commissions.amount, 0)) as total_commissions'),
            ])
                ->whereIn('plans.product_id', $searchIds)
                ->leftJoin('products', 'products.id', '=', 'plans.product_id')
                ->leftJoin('purchases', function ($join) use ($range) {
                    $join->on('plans.id', '=', 'purchases.plan_id')
                        ->where('purchases.created_at', '>', $range['startDate'])
                        ->where('purchases.created_at', '<=', $range['endDate']);
                })
                ->leftJoin('users', 'products.user_id', '=', 'users.id')
//			->leftJoin('visit_products', function($join) use ($range) {
//				$join->on('plans.product_id', '=', 'visit_products.product_id')
//					->where('visit_products.created_at', '>', $range['startDate'])
//					->where('visit_products.created_at', '<=', $range['endDate']);
//			})
                ->leftJoin('transactions', 'purchases.id', '=', 'transactions.purchase_id')
                ->leftJoin('commissions', 'transactions.id', '=', 'commissions.transaction_id')
                ->groupBy('plans.id')
                ->paginate(20);

            $visits = Plan::select([
                'plans.id as plan_id1',
                DB::raw('count(visit_products.id) as visits'),
            ])
                ->whereIn('plans.product_id', $searchIds)
                ->leftJoin('visit_products', function ($join) use ($range) {
                    $join->on('plans.id', '=', 'visit_products.plan_id')
                        ->where('visit_products.created_at', '>', $range['startDate'])
                        ->where('visit_products.created_at', '<=', $range['endDate']);
                })
                ->groupBy('plan_id1')
//			->toSql();
                ->get()->toArray();

            for ($i = 0; $i < count($data); $i++) {
                for ($j = 0; $j < count($visits); $j++) {
                    if ($data[$i]['plan_id1'] == $visits[$j]['plan_id1']) {
                        $data[$i]['visits'] = $visits[$j]['visits'];
                    }
                }
            }

            unset($visits);


            $campaigns = [];

            if (count($data) > 0) {
                foreach ($data as $entry) {
                    $found = false;
                    for ($i = 0; $i < count($campaigns); $i++) {
                        if ($campaigns[$i]['product_id'] == $entry['product_id']) {
                            $found = true;

                            // add sales & counters
                            $campaigns[$i]['total_amount'] += $entry['total_amount'];
                            $campaigns[$i]['sales'] += $entry['sales'];
                            $campaigns[$i]['visits'] += $entry['visits'];
                            $campaigns[$i]['total_commissions'] += $entry['total_commissions'];

                            $campaigns[$i]['plans'][] = $entry;
                        }
                    }

                    if (!$found) {
                        $campaigns[] = [
                            'vendor_name'       => $entry['vendor_name'],
                            'product_id'        => $entry['product_id'],
                            'product_name'      => $entry['product_name'],
                            'total_amount'      => $entry['total_amount'],
                            'sales'             => $entry['sales'],
                            'visits'            => $entry['visits'],
                            'total_commissions' => $entry['total_commissions'],
                            'plans'             => [
                                $entry,
                            ],
                        ];
                    }
                }
            }

            $first_data = $data;
            $data = $campaigns;

            $campaigns = Product::select(['id', 'name'])->whereIn('id', $searchIds)->get()->toArray();

            //Array for sorted campaigns list
            $sorted_campaign_list = [];
            //Generate new array
            foreach ($campaigns as $key => $value) {
                $sorted_campaign_list[$value['name']] = $value;
            }

            ksort($sorted_campaign_list, SORT_NATURAL | SORT_FLAG_CASE);

            $campaigns = $sorted_campaign_list;

            return view('affiliate.report.sales.sales-report', [
                'data'      => $data,
                'campaigns' => $campaigns,
                'startDate' => Carbon::parse($range['startDate'])->format('m/d/Y'),
                'endDate'   => Carbon::parse($range['endDate'])->format('m/d/Y'),
                'query'     => $f_data['query'],
                'f_data'    => $first_data,
            ]);
            */
        }

        /**
         * @param null   $user_id
         * @param string $type
         *
         * @return mixed
         */
        public function getSalesReportLists($user_id = null, $type = 'affiliate')
        {
            $filterMap = [
                'vendor_id'   => 'users.id',
                'campaign_id' => 'products.id',
            ];

            if (!$user_id)
                $user_id = Auth::user()->id;

            $sales_reports = DB::table('plans')
                ->join('product_permissions', 'product_permissions.product_id', '=', 'plans.product_id')
                ->join('products', 'products.id', '=', 'plans.product_id')
                ->join('users', 'users.id', '=', 'products.user_id')
                ->leftJoin(DB::raw('(SELECT product_id, COUNT(product_id) as visits FROM visit_products GROUP BY product_id) as v'), 'v.product_id', '=', 'plans.id')
                ->leftJoin(DB::raw('(SELECT plan_id, COUNT(plan_id) as sales, SUM(amount) AS totalSales FROM purchases GROUP BY plan_id) as p'), 'p.plan_id', '=', 'plans.id')
                ->leftJoin(DB::raw('(SELECT plan_id, SUM(amount) AS totalCommissions FROM commissions GROUP BY plan_id) as c'), 'c.plan_id', '=', 'plans.id')
                ->where('product_permissions.user_id', [$user_id])
                ->whereNotNull('plans.id')
                ->whereIn('product_permissions.promote', ['2', '1'])
                ->distinct()
                ->select('plans.id',
                    'products.id as product_id',
                    'products.name as campaign',
                    DB::raw('CONCAT(users.first_name, " ", users.last_name) AS vendor'),
                    'plans.name as product',
                    'sales',
                    'totalSales',
                    'visits',
                    'totalCommissions',
                    DB::raw('ROUND(sales * 100 / visits) as conversionRate'),
                    DB::raw('ROUND(totalSales * 100 / visits) as epc')
                );

            $search = Input::get('q');
            $filters = $this->getFilters($_GET);
            $main_query = clone $sales_reports;

            if ($search) {
                $sales_reports = $sales_reports->where(function ($query) use ($search) {
                    $query->where('users.first_name', 'like', '%' . $search . '%')
                        ->orWhere('users.last_name', 'like', '%' . $search . '%')
                        ->orWhere('products.name', 'like', '%' . $search . '%')
                        ->orWhere('plans.name', 'like', '%' . $search . '%');
                });
            };

            if (!empty($filters)) {
                foreach ($filters as $index => $filter) {
                    $sales_reports = $sales_reports->where($filterMap[$index], $filter);
                }
            }

            $data['vendors'] = $main_query->select('users.id',
                DB::raw('CONCAT(users.first_name, " ", users.last_name) AS name'))
                ->distinct('users.id')
                ->get();

            $data['campaigns'] = $main_query->select('products.id', 'products.name')
                ->distinct('products.id')
                ->get();

            $data['search'] = $search;
            $data['sales_reports'] = $sales_reports->get();

            // fetch traffic information for campaigns
            $visits = Visit::select(
                [
                    \DB::raw('count(visits.id) as totalVisits'),
                    'product_id',
                ]
            )
                ->leftJoin('visit_products', 'visits.id', '=', 'visit_products.visit_id')
                ->whereIn('visit_products.product_id', Permissions::getAllowedProducts('promote'))
                ->groupBy('visit_products.product_id')
                ->get()->toArray();

            for ($i = 0; $i < count($data['sales_reports']); $i++) {
                $data['sales_reports'][$i]->visits = 0;
                $data['sales_reports'][$i]->eps = number_format(0, 2);
                $data['sales_reports'][$i]->epc = number_format(0, 2);
                $data['sales_reports'][$i]->conversionRate = number_format(0, 2);

                foreach ($visits as $v) {
                    if ($v['product_id'] == $data['sales_reports'][$i]->product_id) {
                        $data['sales_reports'][$i]->visits = $v['totalVisits'];

                        // conversion rate
                        if ($v['totalVisits'] > 0) {
                            $data['sales_reports'][$i]->conversionRate = number_format($data['sales_reports'][$i]->sales * 100 / $v['totalVisits'], 2);
                            $data['sales_reports'][$i]->epc = number_format($data['sales_reports'][$i]->totalSales / $v['totalVisits'], 2);
                        }

                        if ($data['sales_reports'][$i]->sales > 0) {
                            $data['sales_reports'][$i]->eps = number_format($data['sales_reports'][$i]->totalSales / $data['sales_reports'][$i]->sales, 2);
                        }
                    }
                }
            }

            return $data;
        }

        /**
         * Export Sales Reports
         */
        public function exportSalesReports()
        {
            $reports = $this->getSalesReportLists();
            $report_lists = $reports['sales_reports'];

            $sales_reports = [
                [
                    'Vendor',
                    'Ccampaign',
                    'Product',
                    'Clicks',
                    'Visitors',
                    'Sales',
                    'Conversions',
                    'EPC',
                    'EPS',
                    'Total Sales',
                    'Total Commissions',
                ],
            ];
            foreach ($report_lists as $reports) {
                $sales_reports[] = [
                    $reports->vendor,
                    $reports->campaign,
                    $reports->product,
                    0,
                    $reports->visits,
                    $reports->sales,
                    $reports->conversionRate,
                    $reports->epc,
                    0,
                    $reports->totalSales,
                    $reports->totalCommissions,
                ];
            }

            ExportHelper::exportCSVFromList('Affiliate Sales Reports\' List', 'Affiliate Sales Reports', $sales_reports);
        }

        /**
         * Commissions Report
         *
         * @return \Illuminate\View\View|\Wpb\String_Blade_Compiler\StringView
         */
        public function getCommissionsReports()
        {
            $affiliate_id = auth()->user()->id;
            $data = $this->processQueryData();
            $this->_data = $this->getCommissionsReportList($affiliate_id, $data, 10);

            $campaigns = Product::whereIn('products.id', Permissions::getAllowedProducts("reports"))
                ->orderBy("name", "asc")
                ->get();

            //Index for keys with the same Campaign name [Optin Gun_1, Optin Gun_2]
            $i = 0;

            //Array for sorted funnels list
            $sorted_campaigns_list = [];

            //Generate new array with key = 'Campaign name', value = Funnel object
            foreach ($campaigns as $value) {
                $sorted_campaigns_list[$value->name . '_' . $i++] = $value;
            }
            ksort($sorted_campaigns_list, SORT_NATURAL | SORT_FLAG_CASE);

            $this->_data['sorted_campaigns_list'] = $sorted_campaigns_list;

            $this->_data['queryString'] = Input::get('q', '');

            return View::make('affiliate.report.commissions.commissions-report', $this->_data);
        }

        /**
         * Get Commissions report list
         *
         * @return view
         */
        public function getCommissionsReportList($affiliate_id, $data, $pagination = 10)
        {
            $range = GeneralHelper::getDateRange();

            $data['startDate'] = Carbon::parse($range['startDate'])->format('m/d/Y');
            $data['endDate'] = Carbon::parse($range['endDate'])->format('m/d/Y');
            $data['filters'] = array_merge($data['filters'], $range);

            // Get all affiliates from the Visits (traffic) table
            $data['reports'] = ReportsHelper::getCommissionsReports($data, $pagination, $affiliate_id);

            return $data;
        }

        /**
         * @param null   $user_id
         * @param string $type
         *
         * @return mixed
         */
        public function getCommissions($user_id = null, $type = 'affiliate')
        {
            if (!$user_id) {
                $user_id = Auth::user()->id;
            }
            $query = Purchase::
            join('plans', 'purchases.plan_id', '=', 'plans.id')
                ->join('products', 'plans.product_id', '=', 'products.id')
                ->join('users as u', 'commissions.affiliate_id', '=', 'u.id')
                ->join('users as v', 'products.user_id', '=', 'v.id')
                ->join('transactions', 'purchases.id', '=', 'transactions.purchase_id')
                ->leftJoin('commissions', 'transactions.id', '=', 'commissions.transaction_id')
                ->where('transactions.is_approved', 1)
                ->select([
                    DB::raw('FORMAT(transactions.amount, 2) as price'),
                    DB::raw('FORMAT(commissions.amount, 2) as commissions'),
                    DB::raw('FORMAT(commissions.percent, 2) as percent'),
                    'is_refund',
                    'products.name as campaign',
                    'plans.name as product',
                    'u.first_name',
                    'u.last_name',
                    'u.email',
                    'v.first_name as v_first_name',
                    'v.last_name as v_last_name',
                    'transactions.created_at as date',
                ])
                ->orderBy('transactions.created_at', 'desc');

            if ($type == 'admin') {
                $query->where('products.user_id', [$user_id]);
            } else {
                $query->where('commissions.affiliate_id', [$user_id]);
            }

            $filterMap = [
                'campaign_id' => 'products.id',
                'status'      => 'transactions.is_refund',
            ];

            $search = Input::get('q');
            $filters = $this->getFilters($_GET);
            $main_query = clone $query;

            if ($search) {
                $query = $query->where(function ($q) use ($search) {
                    $q->where('u.first_name', 'like', '%' . $search . '%')
                        ->orWhere('u.last_name', 'like', '%' . $search . '%')
                        ->orWhere('u.email', 'like', '%' . $search . '%')
                        ->orWhere('v.first_name', 'like', '%' . $search . '%')
                        ->orWhere('v.last_name', 'like', '%' . $search . '%')
                        ->orWhere('products.name', 'like', '%' . $search . '%')
                        ->orWhere('plans.name', 'like', '%' . $search . '%');
                });
            };

            if (!empty($filters)) {
                foreach ($filters as $index => $filter) {
                    if (isset($filterMap[$index])) {
                        $query = $query->where($filterMap[$index], $filter);
                    }
                }
            }

            if (isset($filters['startDate']) && isset($filters['endDate'])) {

                //Convert date format from m/d/Y to mysql datetime Y/m/d H:i:s
                $from = Carbon::parse($filters['startDate'])->format('Y/m/d H:i:s');
                $to = Carbon::parse($filters['endDate'])->format('Y/m/d H:i:s');

                $query = $query->where('transactions.created_at', '>=', $from)
                    ->where('transactions.created_at', '<', $to);
            }

            $data['campaigns'] = $main_query->select('products.id', 'products.name')
                ->distinct('products.id')
                ->get();

            $data['search'] = $search;
            $data['commissions'] = $query;

            return $data;
        }

        /**
         * Export Commissions Reports
         */
        public function exportCommissionsReports()
        {
            $affiliate_id = auth()->user()->id;

            $data = $this->processQueryData();
            $reports = $this->getCommissionsReportList($affiliate_id, $data, 10);

            $report_lists = $reports['reports'];

            $comm_reports = [
                [
                    'Date',
                    'Customer',
                    'Customer Email',
                    'Vendor',
                    'Campaign',
                    'Product',
                    'Status',
                    'Amount',
                    'Commission amount',
                    'Commission %',
                ],
            ];


            foreach ($report_lists as $reports) {
                $comm_reports[] = [
                    date('m/d/Y', strtotime($reports['created_at'])) . ' (' . date('h:i A', strtotime($reports['created_at'])) . ')',
                    $reports['buyer_first_name']. ' ' . $reports['buyer_last_name'],
                    $reports['buyer_email'],
                    $reports['v_first_name'] . ' ' . $reports['v_last_name'],
                    $reports['product_name'],
                    $reports['plan_name'],
                    $reports['com_status'] == 2 ? "Paid" : "Pending",
                    '$' . ($reports['amount']) ? $reports['amount'] : 0,
                    '$' . ($reports['com_amount']) ? $reports['com_amount'] : 0,
                    ($reports['com_percent']) ? $reports['com_percent'] . '%' : 0 . '%',
                ];
            }

            ExportHelper::exportCSVFromList('Affiliate Commissions Reports\' List', 'Affiliate Commissions Reports', $comm_reports);
        }

        public function getLinkStatsReports ()
        {
            /** @var User $auth_user */
            $auth_user = auth ()->user ();

            $affiliate_id = $auth_user->id;

            $query_data = $this->processQueryData ();

            $range = GeneralHelper::getDateRange ();
            $query_data[ 'filters' ]['startDate'] = Carbon::parse ($range[ 'startDate' ])->format ('Y-m-d 00:00:00');
            $query_data[ 'filters' ]['endDate'] = Carbon::parse ($range[ 'endDate' ])->format ('Y-m-d 23:59:59');

            $this->_data = [
                'query'       => $query_data[ 'searchParams' ],
                'searchQuery' => Input::get ('q', ''),
                'startDate'   => Carbon::parse ($range[ 'startDate' ])->format ('m/d/Y'),
                'endDate'     => Carbon::parse ($range[ 'endDate' ])->format ('m/d/Y'),
                'filters'     => array_merge ($query_data[ 'filters' ], $range),
                'reports'     => ReportsHelper::getLinkStatsReports ($query_data, 10, $affiliate_id),
            ];

            $allowed_product_ids = array_unique(array_filter(array_merge(Permissions::getAllowedProducts ("promote"), Permissions::getAllowedProducts ("reports"))));
            $campaigns = Product::whereIn ('products.id', $allowed_product_ids)
                ->orderBy ("name", "asc")
                ->get ();

            $sorted_campaigns_list = [];

            foreach ($campaigns as $i => $value) {
                $sorted_campaigns_list[ $value->name . '_' . ($i + 1) ] = $value;
            }

            ksort ($sorted_campaigns_list, SORT_NATURAL | SORT_FLAG_CASE);

            $this->_data[ 'sorted_campaigns_list' ] = $sorted_campaigns_list;

            return View::make ('affiliate.report.linkstats.report', $this->_data);
        }

        /**
         * Get 2nd Tier affiliates
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function getSecondTierAffiliates()
        {
            $query_data = $this->processQueryData ();

            if ( !isset($query_data['filters']['startDate']) && !isset($query_data['filters']['endDate'])) {
                $query_data['filters']['startDate'] =  Carbon::createFromDate(2016, 1, 1)->format ('Y-m-d 00:00:00');
                $query_data['filters']['endDate']   = Carbon::now()->format('Y-m-d 23:59:59');
            } else {
                $range = GeneralHelper::getDateRange ();
                $query_data['filters']['startDate'] = Carbon::parse ($range['startDate'])->format ('Y-m-d 00:00:00');
                $query_data['filters']['endDate']   = Carbon::parse ($range['endDate'])->format ('Y-m-d 23:59:59');
            }

            // Campaigns ids selected in filter
            $filtered_campaigns_id = isset($query_data['filters']['campaign']) ? (array) $query_data['filters']['campaign'] : null;

            // Get ids of products that current user is affiliate for
            $products_ids_lvl2 = Permissions::getAllowedProducts('promote', 2);
            $products_ids_lvl1 = Permissions::getAllowedProducts('promote', 1);
            $products_ids      = array_merge($products_ids_lvl1, $products_ids_lvl2);

            // All campaigns for filter
            $all_campaigns = Product::whereIn('id', $products_ids)->orderBy('name')->get();

            // Get filtered campaigns by ids
            $campaigns = Product::whereIn('id', $products_ids)
                ->where(function ($query) use ($filtered_campaigns_id) {
                    if ($filtered_campaigns_id) {
                        $query->whereIn('id', $filtered_campaigns_id);
                    }
                })
                ->orderBy('name')
                ->get()->pluck('id')->toArray();

            // Get all affiliates that current affiliate is a 2nd Tier Affiliate for.
            $affiliates = AffiliateRequest::where('level2_aff_id', auth()->id())
                ->whereIn('product_id', $campaigns)
                ->where('status', 1)
                ->get();

            $affiliates_map = [];
            foreach ($affiliates as $affiliate) {
                $affiliates_map[] = $affiliate->affiliate_id .'_'. $affiliate->product_id;
            }

            // Delete affiliates id duplicates (case when affiliate sent requests for multiple products)
            $affiliates_map = array_unique($affiliates_map);

            // Fetch all commissions that current affiliate made
            $info_and_commissions = Commission::fetchAffiliatesInfoAndCommissions($campaigns, $query_data['filters']);

            $commissions = [];
            foreach ($info_and_commissions as $item) {
                if (empty($commissions[$item->tier_1_affiliate_id][$item->product_id])) {
                    $commissions[$item->tier_1_affiliate_id][$item->product_id] = [
                        'amount'           => $item->tier_2_amount,
                        'count'            => 1,
                        'product_name'     => $item->product_name,
                        'product_logo'     => $item->product_logo,
                        'affiliate_avatar' => $item->affiliate_avatar,
                        'affiliate_email'  => $item->affiliate_email,
                        'affiliate_name'   => $item->affiliate_name
                    ];
                } else {
                    $commissions[$item->tier_1_affiliate_id][$item->product_id]['amount'] += $item->tier_2_amount;
                    $commissions[$item->tier_1_affiliate_id][$item->product_id]['count']  += 1;
                }
                if(in_array($item->tier_1_affiliate_id .'_'. $item->product_id, $affiliates_map)) {
                    unset($affiliates_map[array_search($item->tier_1_affiliate_id .'_'. $item->product_id, $affiliates_map)]);
                }

            }

            if ( !empty($affiliates_map)) {
                foreach ($affiliates_map as $item) {
                    $aff_id  = explode('_', $item)[0];
                    $prod_id = explode('_', $item)[1];

                    $tmp_user = User::find($aff_id);
                    $tmp_prod = Product::find($prod_id);

                    if (is_null($tmp_user) || is_null($tmp_prod)) { continue; }

                    $commissions[$aff_id][$prod_id] = [
                        'amount'           => 0,
                        'count'            => 0,
                        'product_name'     => $tmp_prod->name,
                        'product_logo'     => $tmp_prod->dark_logo,
                        'affiliate_avatar' => $tmp_user->image,
                        'affiliate_email'  => $tmp_user->email,
                        'affiliate_name'   => $tmp_user->first_name .' '. $tmp_user->last_name
                    ];
                }
            }

            $data = [
                'query'         => $query_data[ 'searchParams' ],
                'searchQuery'   => Input::get ('q', ''),
                'startDate'     => date('m/d/Y', strtotime($query_data['filters']['startDate'])),
                'endDate'       => date('m/d/Y', strtotime($query_data['filters']['endDate'])),
                'commissions'   => $commissions,
                'commissions_paginate' => $info_and_commissions,
                'all_campaigns' => $all_campaigns->pluck('name', 'id')
            ];

            return view('affiliate.report.second_tier_affiliates.index', $data);
        }


        /**
         * Get 2nd Tier affiliates by affiliate id
         *
         * @param $affiliate_id
         * @param $campaign_id
         * @return array
         * @throws \Throwable
         */
        public function getSecondTierAffiliatesByAffiliateId($affiliate_id, $campaign_id)
        {
            // Get all affiliates that current affiliate is a 2nd Tier Affiliate for.
            $affiliates = (new AffiliateRequest)->where('level2_aff_id', $affiliate_id)
                ->where('product_id', $campaign_id)
                ->where('status', 1)
                ->get();

            $affiliates_map = [];
            foreach ($affiliates as $affiliate) {
                $affiliates_map[] = $affiliate->affiliate_id .'_'. $affiliate->product_id;
            }

            // Delete affiliates id duplicates (case when affiliate sent requests for multiple products)
            $affiliates_map = array_unique($affiliates_map);

            // Fetch all commissions
            $info_and_commissions = Commission::fetchAffiliatesInfoAndCommissions([$campaign_id], null, $affiliates->pluck('affiliate_id')->toArray());

            $commissions = [];

            foreach ($info_and_commissions as $item) {
                if (empty($commissions[$item->tier_1_affiliate_id][$item->product_id])) {
                    $commissions[$item->tier_1_affiliate_id][$item->product_id] = [
                        'amount'           => $item->tier_2_amount,
                        'count'            => 1,
                        'product_name'     => $item->product_name,
                        'product_logo'     => $item->product_logo,
                        'affiliate_avatar' => $item->affiliate_avatar,
                        'affiliate_email'  => $item->tier_1_affiliate_email,
                        'affiliate_name'   => $item->tier_1_affiliate_first_name . ' ' . $item->tier_1_affiliate_last_name
                    ];
                } else {
                    $commissions[$item->tier_1_affiliate_id][$item->product_id]['amount'] += $item->tier_2_amount;
                    $commissions[$item->tier_1_affiliate_id][$item->product_id]['count']  += 1;
                }
                if(in_array($item->tier_1_affiliate_id .'_'. $item->product_id, $affiliates_map)) {
                    unset($affiliates_map[array_search($item->tier_1_affiliate_id .'_'. $item->product_id, $affiliates_map)]);
                }
            }

            if ( !empty($affiliates_map)) {
                foreach ($affiliates_map as $item) {
                    $aff_id  = explode('_', $item)[0];
                    $prod_id = explode('_', $item)[1];

                    $tmp_user = User::find($aff_id);
                    $tmp_prod = Product::find($prod_id);

                    if (is_null($tmp_user) || is_null($tmp_prod)) { continue; }

                    $commissions[$aff_id][$prod_id] = [
                        'amount'           => 0,
                        'count'            => 0,
                        'product_name'     => $tmp_prod->name,
                        'product_logo'     => $tmp_prod->dark_logo,
                        'affiliate_avatar' => $tmp_user->image,
                        'affiliate_email'  => $tmp_user->email,
                        'affiliate_name'   => $tmp_user->first_name .' '. $tmp_user->last_name
                    ];
                }
            }

            $html = view('admin.affiliates.modals.affiliate_2nd_content', [
                'commissions' => $commissions,
                'commissions_paginate' => $info_and_commissions,
            ])->render();

            return [
                'success' => 1,
                'html'    => $html
            ];
        }
    }
