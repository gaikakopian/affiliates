
var BASE_URL = window.location.origin;

// ZERO Clipboard
ZeroClipboard.setDefaults( { moviePath: BASE_URL + '/js/ZeroClipboard.swf' } );

$(function() {

    // ZERO Clipboard
    var clip = new ZeroClipboard( $('.d_clip_button') );

    clip.on( 'complete', function(client, args) {
        var t = $(this);
        t.text('Copied');
    });

    // Bootstrap Datepicker
    $('.datepicker').datepicker({
        'format' : 'mm/dd/yyyy'
    });

    // Show/Hide sections
    $('.manageSection').click(function(e){
        var section = $(e.currentTarget).attr('data-section');
        var section_child = section + '_child';

        if(section == 'is_recurring')
        {
            if($("input[name='is_recurring']").prop("checked"))
            {
                $("input[name='has_split_pay']").prop( "checked", false );
                $('#has_split_pay_child').addClass('hide');
                $('#has_split_pay_section').addClass('hide');
            }
            else
            {
                $('#has_split_pay_section').removeClass('hide');
            }
        }

        $('#' + section_child).toggleClass('hide');
    });
});

$(document).ready(function(){

    $(document).ready(function(){
        $('#selecctall').click(function(event){
            if(this.checked){
                $('.checkbox1').attr({checked: "checked"});
            }else{
                $('.checkbox1').removeAttr('checked');
            }
        });

        $(document).on('change', '.checkbox1', function(event) {
            if($('#selecctall').is(':checked')){
                $('#selecctall').removeAttr('checked');
            }
        });
    });

        // $('#req_freq').show();
    // var showRec = $('#prod_type').attr('data-recurring');

    var showRec = $('#prod_type').attr('data-recurring');

    if(showRec == '1' || $('#prod_type').val() == '1'){
        // $("#prod_type option[value='1']").attr('selected','selected');
        $('#req_freq').show();
        $('#paypal_sub').show();
    }

    else{
        $('#req_freq').hide();
        $('#paypal_sub').hide();
    }

    $('#prod_type').change(function() {
        var selectedValue = $('#prod_type').find(":selected").val();
        if(selectedValue == '0'){
            $('#req_freq').hide();
            $('#paypal_sub').hide();
        }
        else{
            $('#req_freq').show();
            $('#paypal_sub').show();
        }
    });
});

// Hide Permissions when Super admin checkbox is checked.
// New/ Edit User

$(document).ready(function($) {
	if($('#super_admin').is(":checked")) {
		$('#permissions').hide();
	} else {
		$('#permissions').show();
	}
});

$('#super_admin').change(function(event) {
    if($(this).is(":checked")) {
        var returnVal = confirm("Are you sure to make this user 'Super Admin'?");
        $(this).attr("checked", returnVal);
        $('#permissions').hide();
    } else {
        $('#permissions').show();
    }
});

$('#all_affiliates').click(function() {
    if( $(this).is(':checked')) {
        $("#select_affiliates").hide();
    } else {
        $("#select_affiliates").show();
    }
});

$(document).ready(function() {
    if( $('#all_affiliates').is(':checked')) {
        $("#select_affiliates").hide();
    } else {
        $("#select_affiliates").show();
    }
});

$(document).ready(function(){
    $("input[name$='has_license']").click(function() {
        var test = $(this).val();
        $("div.showUsage").toggle();
    });
});

$(document).ready(function(){
    $("input[name$='has_split_pay']").click(function() {
        var test = $(this).val();
        $("div.showSpilitOption").toggle();
    });
});

$(function(){
    $("#mt_product_id").change(function(e){
        var product_id = $(e.currentTarget).val();

        var plan_menu = $('#mt_plan_id');

        // Clear all old values
        plan_menu.empty();

        // Show loading
        plan_menu.append($('<option>').text('Loading...').attr('value', ''));

        $.ajax({
          type: "GET",
          url: BASE_URL + '/admin/campaigns/get-products-by-campaign/' + product_id,
          data: '',
          dataType: 'json',
          success: function(plans)
          {
            // Clear all old values
            plan_menu.empty();

            // Add first option
            plan_menu.append($('<option>').text('Select').attr('value', ''));

            // Loop all other values
            $.each(plans, function( index, value )
            {
              plan_menu.append($('<option>').text(value).attr('value', index));
            });

            plan_menu.selectpicker('refresh');
          }
        });
    });
});

$(document).ready(function() {
    $('#email_html_div').hide();
});
onARCodeKeyUp();
function onARCodeKeyUp() {
        $('#form_code').keyup(function(){
            splitARCode();
            // console.log('ok');
            return false;
        });
    }

$(document).ready(function() {
        var service_name = $('#email_service').val();
        if(service_name == 'html_code')
        {
            $('#email_html_div').show();
            $(".ar-other-code").show();
            splitARCode();
            $('.email_list').hide();
            return;
        }
        else
        {
            $(".ar-other-code").hide();
            $('.email_list').show();
            $('#email_html_div').hide();
        }
});

$(function(){
    $("#email_service").change(function(e){
        var service_name = $(e.currentTarget).val();

        var email_list = $('#email_list');

        if(service_name == 'html_code')
        {
            $('#email_html_div').show();
            $(".ar-other-code").show();
            splitARCode();
            $('.email_list').hide();
            return;
        }
        else
        {
            $(".ar-other-code").hide();
            $('.email_list').show();
            $('#email_html_div').hide();
        }
        email_list.show();

        // Clear all old values
        email_list.empty();

        if(service_name.length !== 0){
            // Show loading
            email_list.append($('<option>').text('Loading...').attr('value', ''));

            $.ajax({
              type: "GET",
              url: BASE_URL + '/admin/getLists/' + service_name,
              data: '',
              dataType: 'json',
              success: function(lists)
              {
                // Clear all old values
                email_list.empty();

                // Add first option
                email_list.append($('<option>').text('Select').attr('value', ''));

                // Loop all other values
                $.each(lists, function( index, value )
                {

                  email_list.append($('<option>').text(value.name).attr('value', value.id));
                });
              }
            });
        }
    });
});


$( document ).ready(function(){
    if ($("#all_affiliates").is(':checked')){
        $("#affsSelect").hide();
    }
    else
    {
        $('#affsSelect').show();
    }
});

$('#all_affiliates').change(function(){
    $('#affsSelect').toggle();
});

    // Override Commission Add
    $(".addOverrideForm").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                // console.log(data);
                if(typeof(data.success) != 'undefined')
                {

                    $('.alert_error').hide();
                    $('.alert_success').removeClass('hide');
                    // resetForm($('#addOverrideForm'));
                }
                else
                {
                    $('.alert_error').removeClass('hide');
                    err_affiliate_exist = (typeof(data.exist) != 'undefined') ? 'Affiliate already added to this plan.' : "";
                    err_affiliate_id = (typeof(data.affiliate_id) != 'undefined') ? data.affiliate_id : "";
                    err_plan_id = (typeof(data.plan_id) != 'undefined') ? data.plan_id : "";
                    err_level1 = (typeof(data.level1) != 'undefined') ? data.level1 : "";
                    err_level2= (typeof(data.level2) != 'undefined') ? data.level2 : "";
                    $('.valid_err').html('<p>'+err_affiliate_exist+'</p>'+'<p>'+err_affiliate_id+'</p>'+'<p>'+err_plan_id+'</p>'+'<p>'+err_level1+'</p>'+'<p>'+err_level2+'</p>');
                }

                $('.close_btn').click(function() {
                    location.reload();
                });
            },
        });
    e.preventDefault(); //STOP default action
    // e.unbind('submit'); //unbind. to stop multiple form submit.
});

    // Override Edit Commission
var override_data = {};
var overrideModalID;

$('.overrideEditBtn').click(function(e){

    override_data.idNumb = $(this).attr("data-override-id");
    override_data.affiliate_id = $(this).attr("data-affiliate-id");
    override_data.plan_id      = $(this).attr("data-plan-id");
    override_data.lvlOne       = $(this).attr("data-lvlOne");
    override_data.lvlTwo       = $(this).attr("data-lvlTwo");

    overrideModalID = '#overrideEditModal'+ override_data.idNumb;

$(overrideModalID).on('show.bs.modal', function (e) {

        $('.affiliate_id').val(override_data.affiliate_id);
        $('.plan_id').val(override_data.plan_id);
        $('.overrideLvlOne').val(override_data.lvlOne);
        $('.overrideLvlTwo').val(override_data.lvlTwo);
    });

});

    $(".overrideEditForm").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(typeof(data.success) != 'undefined')
                {
                    $('.alert_error').hide();
                    $('.alert_success').removeClass('hide');
                }
                else
                {
                    $('.alert_error').removeClass('hide');
                    err_affiliate_exist = (typeof(data.exist) != 'undefined') ? 'Affiliate already added to this plan.' : "";
                    err_affiliate_id = (typeof(data.affiliate_name) != 'undefined') ? data.affiliate_name : "";
                    err_plan_id = (typeof(data.plan_name) != 'undefined') ? data.plan_name : "";
                    err_level1 = (typeof(data.level1) != 'undefined') ? data.level1 : "";
                    err_level2 = (typeof(data.level2) != 'undefined') ? data.level2 : "";
                    $('.valid_err').html('<p>'+err_affiliate_exist+'</p>'+'<p>'+err_affiliate_id+'</p>'+'<p>'+err_plan_id+'</p>'+'<p>'+err_level1+'</p>'+'<p>'+err_level2+'</p>'+'<p>');
                }

                $('.close_btn').click(function() {
                    location.reload();
                });
            },
        });
    e.preventDefault(); //STOP default action
    // e.unbind('submit'); //unbind. to stop multiple form submit.
});

$('#create_aff_campaign').click(function(event) {

	$('#affiliate_id_add_campaign').val('');
	$('#product_id_add_campaign').val('');
	$('#campaign_name_add_campaign').val('');
	$('#destination_url_add_campaign').val('');
	$('#submit_add_campaign').prop("disabled", false);

});

    //callback handler for form submit
    $(".trackingLinkAddForm").submit(function(e)
    {
        $('#submit_add_campaign').prop("disabled", true);
        $('.close_btn').hide();

        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(typeof(data.success) != 'undefined')
                {
                    $('.close_btn').show();
                    $('.alert_error').hide();
                    $('.alert_success').removeClass('hide');
                }
                else
                {
                    $('.close_btn').show();
                    $('#submit_add_campaign').prop("disabled", false);
                    $('.alert_error').removeClass('hide');
                    err_campaign_name = (typeof(data.campaign_name) != 'undefined') ? data.campaign_name : "";
                    err_product = (typeof(data.product_id) != 'undefined') ? data.product_id : "";
                    err_affiliate_id = (typeof(data.affiliate_id) != 'undefined') ? data.affiliate_id : "";
                    $('.valid_err').html('<p>'+err_campaign_name+'</p>'+'<p>'+err_product+'</p>'+'<p>'+err_affiliate_id+'</p>');
                }

                $('.close_btn').click(function() {
                    location.reload();
                });
            },
        });
    e.preventDefault(); //STOP default action
});


var campaign_data = {};
var linkModalID;

$('.cmpgnEditBtn').click(function(e){


	campaign_data.idNumb       		= $(this).attr("data-link-id");
	campaign_data.product_name       = $(this).attr("data-product-name");
	campaign_data.campaign_name       = $(this).attr("data-campaign-name");
	campaign_data.affiliate_id       = $(this).attr("data-affiliate-id");
	campaign_data.destination_url       = $(this).attr("data-campaign-destinationurl");


        linkModalID = '#editModal'+ campaign_data.idNumb;


	$(linkModalID).on('show.bs.modal', function (e) {
		$('#product_name').val(campaign_data.product_name);
		$('.campaign_name').val(campaign_data.campaign_name);
		$('.destination_url').val(campaign_data.destination_url);
		$('#affiliate_id').val(campaign_data.affiliate_id);
	});


});

$('.cmpgnEditBtn').click(function(event) {
    $('#submit').prop("disabled", false);
});

//callback handler for form submit
$(".trackingLinkEditForm").submit(function(e)
{
    $('#submit').prop("disabled", true);
    $('.close_btn').hide();
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");


    $.ajax(
    {
        url : formURL,
        type: "POST",
        dataType: 'JSON',
        data : postData,
        success:function(data)
        {
            if(typeof(data.success) != 'undefined')
            {
                $('.close_btn').show();
                $('.alert_error').hide();
                $('.alert_success').removeClass('hide');
                $('#submit').prop("disabled", false);
            }
            else
            {
                $('#submit').prop("disabled", false);

                $('.alert_error').removeClass('hide');
                err_campaign_name = (typeof(data.campaign_name) != 'undefined') ? data.campaign_name : "";
                err_product = (typeof(data.product_id) != 'undefined') ? data.product_id : "";
                $('.valid_err').html('<p>'+err_campaign_name+'</p>'+'<p>'+err_product+'</p>');
            }

            $('.close_btn').click(function() {
                location.reload();
            });
        },
    });
    e.preventDefault(); //STOP default action
    // e.unbind('submit'); //unbind. to stop multiple form submit.
});

$(document).ready(function() {
    $('#EmailButton').hide();
    $('#ImageButton').hide();
});
$('#material_type').change(function() {
    var selectedValue = $('#material_type').find(":selected").val();
    // console.log(selectedValue);
    if(selectedValue == 'email'){
        $('#EmailButton').show();
        $('#banner_url').remove();
        var email_text = $('#email_text').length;
        // console.log('email_text='+email_text);
        if (!email_text)
        {
            $('#email_container').prepend('<textarea type="text" class="form-control" name="email_text" id="email_text" rows="15"></textarea>');
        }
        $('#ImageButton').hide();
    }
    else if(selectedValue == 'banner')
    {
        $('#ImageButton').show();
        $('#email_text').remove();
        if (!$('#banner_url').length)
        {
            // console.log('banner_url');
            $('#banner_container').append('<input type="text" class="form-control" name="banner" id="banner_url">');
        }
        $('#EmailButton').hide();
    }
    else{
        $('#EmailButton').hide();
        $('#ImageButton').hide();
    }
});


$('#add_email_swipe').click(function(event) {
    $('#product_name').val('');
    $('#campaign_title').val('');
    $('#notes').val('');
    $('#email_text').val('');
    $('#submit').prop("disabled", false);
});

$(".emailAdd").submit(function(e)
    {
    $('#submit').prop("disabled", true);
    $('.close_btn').hide();

        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(typeof(data.success) != 'undefined')
                {
                    $('.close_btn').show();
                    $('.alert_error').hide();
                    $('.alert_success').removeClass('hide');
                }
                else
                {
                    $('#submit').prop("disabled", false);
                    $('.close_btn').show();

                    $('.alert_error').removeClass('hide');
                    err_product_name = (typeof(data.product_name) != 'undefined') ? data.product_name : "";
                    err_campaign_title = (typeof(data.campaign_title) != 'undefined') ? data.campaign_title : "";
                    err_email_text = (typeof(data.email_text) != 'undefined') ? data.email_text : "";
                    $('.valid_err').html('<p>'+err_product_name+'</p>'+'<p>'+err_campaign_title+'</p>'+err_email_text);
                }


                $('.close_btn').click(function() {
                    location.reload();
                });
            },
        });
    e.preventDefault(); //STOP default action
    // e.unbind('submit'); //unbind. to stop multiple form submit.
});

$("#add_banner").click(function(event) {
    /* Act on the event */
    $('#product_name').val('');
    $('#campaign_title').val('');
    $('#notes').val('');
    $('#banner').val('');
    $('#add_banner_submit').prop("disabled", false);
});

$(".bannerAdd").submit(function(e)
{
    $('#add_banner_submit').prop("disabled", true);
    $('.close_btn').hide();

        //var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        var postData = new FormData(this);
        $.ajax(
        {
            url : formURL,
            type: "POST",
            cache: false,
            processData: false, 
            contentType: false, 
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(typeof(data.success) != 'undefined')
                {
                    $('.close_btn').show();
                    $('.alert_error').hide();
                    $('.alert_success').removeClass('hide');

                }
                else
                {
                    $('#add_banner_submit').prop("disabled", false);
                    $('.close_btn').show();

                    $('.alert_error').removeClass('hide');
                    err_product_name = (typeof(data.product_name) != 'undefined') ? data.product_name : "";
                    err_campaign_title = (typeof(data.campaign_title) != 'undefined') ? data.campaign_title : "";
                    err_banner = (typeof(data.banner) != 'undefined') ? data.banner : "";
                    $('.valid_err').html('<p>'+err_product_name+'</p>'+'<p>'+err_campaign_title+'</p>'+err_banner+'</p>');
                }

                $('.close_btn').click(function() {
                    location.reload();
                });
            },
        });
    e.preventDefault(); //STOP default action
    // e.unbind('submit'); //unbind. to stop multiple form submit.
});

    var banner_data = {};
    var bannerModalID;

    $('.bannerEditBtn').click(function(e){
        $('.edit_banner_submit').prop("disabled", false);

        banner_data.idNumb         = $(this).attr("data-promo-id");
        banner_data.product_name   = $(this).attr("data-product-name");
        banner_data.campaign_title = $(this).attr("data-campaign-title");
        banner_data.notes          = $(this).attr("data-banner-note");
        banner_data.banner_url     = $(this).attr("data-banner-url");

        bannerModalID = '#bannerEditModal'+ banner_data.idNumb;
        // console.log(banner_data);

    $(bannerModalID).on('show.bs.modal', function (e) {
        $('.product_id').val(banner_data.product_name);
        $('.campaign_title').val(banner_data.campaign_title);
        $('.notes').val(banner_data.notes);
        $('.banner_url').val(banner_data.banner_url);
    });
});



    $(".editBannerForm").submit(function(e)
    {
        $('.edit_banner_submit').prop("disabled", true);
        $('.close_btn').hide();

        //var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        var postData = new FormData(this);
        $.ajax(
        {
            url : formURL,
            type: "POST",
            cache: false,
            processData: false, 
            contentType: false, 
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(typeof(data.success) != 'undefined')
                {
                    $('.close_btn').show();
                    $('.alert_error').hide();
                    $('.alert_success').removeClass('hide');
                    $('.edit_banner_submit').prop("disabled", false);
                }
                else
                    {
                        $('.edit_banner_submit').prop("disabled", false);
                        $('.close_btn').show();

                        $('.alert_error').removeClass('hide');
                        err_product_name = (typeof(data.product_name) != 'undefined') ? data.product_name : "";
                        err_campaign_title = (typeof(data.campaign_title) != 'undefined') ? data.campaign_title : "";
                        err_notes = (typeof(data.notes) != 'undefined') ? data.notes : "";
                        err_banner = (typeof(data.banner) != 'undefined') ? data.banner : "";
                        $('.valid_err').html('<p>'+err_product_name+'</p>'+'<p>'+err_campaign_title+'</p>'+'<p>'+err_notes+'</p>'+'<p>'+err_banner+'</p>');
                    }

                    $('.close_btn').click(function() {
                        location.reload();
                    });
                },
            });
    e.preventDefault(); //STOP default action
    // e.unbind('submit'); //unbind. to stop multiple form submit.
});

$(".editEmailForm").submit(function(e)
{
    $('.submit_edit_email').prop("disabled", true);
    $('.close_btn').hide();
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
        dataType: 'JSON',
        data : postData,
        success:function(data)
        {
            if(typeof(data.success) != 'undefined')
            {
                $('.close_btn').show();
                $('.alert_error').hide();
                $('.alert_success').removeClass('hide');
                $('.submit_edit_email').prop("disabled", false);
            }
            else
                {
                    $('.submit_edit_email').prop("disabled", false);
                    $('.close_btn').show();

                    $('.alert_error').removeClass('hide');
                    err_product_name = (typeof(data.product_name) != 'undefined') ? data.product_name : "";
                    err_campaign_title = (typeof(data.campaign_title) != 'undefined') ? data.campaign_title : "";
                    err_notes = (typeof(data.notes) != 'undefined') ? data.notes : "";
                    err_email_text = (typeof(data.email_text) != 'undefined') ? data.email_text : "";
                    $('.valid_err').html('<p>'+err_product_name+'</p>'+'<p>'+err_campaign_title+'</p>'+'<p>'+err_notes+'</p>'+'<p>'+err_email_text+'</p>');
                }

                $('.close_btn').click(function() {
                    location.reload();
                });
            },
        });
        e.preventDefault(); //STOP default action
        // e.unbind('submit'); //unbind. to stop multiple form submit.
    });

/**
    * Get plans via product id
    * in search filters
    */
    $(function(){

        $("#product_id").change(function(e){

            var product_id = $(e.currentTarget).val();
            var plan_menu = $('#plan_id');

        // Clear all old values
        plan_menu.empty();

        // Show loading
        plan_menu.append($('<option>').text('Loading...').attr('value', ''));

        if(product_id != "")
        {
            $.ajax({
                type: "GET",
                url: BASE_URL + '/get-plans/' + product_id,
                data: '',
                dataType: 'json',
                success: function(plans)
                {
            // Clear all old values
            plan_menu.empty();

            // Add first option
            plan_menu.append($('<option>').text('Select').attr('value', ''));

            // Loop all other values
            $.each(plans, function( index, value )
            {
                plan_menu.append($('<option>').text(value.plan_name).attr('value', value.id));
            });
          }
        });
        }
        else{
            plan_menu.empty();
        }
    });
    });

    $(document).ready(function () {

        /*if ({{ Input::old('autoOpenAdd', 'false') }}) {
            $('#payableModal{{Input::old('modalId')}}').modal('show');
        }*/


    });

    var affiliate_pay_data = {};

    $('.affPayBtn').click(function(e){
        $('#partialAmount').val('');
        $('#payThrough').val('');
        $('#submit').prop("disabled", false);
        affiliate_pay_data.affiliate_id     = $(this).attr("data-affiliate-id");
        affiliate_pay_data.affiliate_name   = $(this).attr("data-affiliate-name");
        affiliate_pay_data.affiliate_amount = $(this).attr("data-affiliate-amount");

    });

    $('#payableModal').on('show.bs.modal', function (e) {
        $('#aff_full_amount').val(affiliate_pay_data.affiliate_amount);
        $('#affiliate_id').val(affiliate_pay_data.affiliate_id);
        $('#aff_name').html(affiliate_pay_data.affiliate_name);
    });

    $(".payoutForm").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        var paid_amount =  affiliate_pay_data.affiliate_amount - $('#partialAmount').val();
        $('#submit').attr('disabled', 'disabled');

        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(typeof(data.success) != 'undefined')
                {
                    $('#aff_full_amount').val(paid_amount);
                    $('.alert_error').hide();
                    $('.alert_success').removeClass('hide');
                        // plan_menu.empty();
                    }
                    else
                    {
                    $('#submit').prop("disabled", false);

                        $('.alert_error').removeClass('hide');
                        err_amount = (typeof(data.amount) != 'undefined') ? data.amount : "";
                        err_payThrough = (typeof(data.payThrough) != 'undefined') ? data.payThrough : "";
                        $('.valid_err').html('<p>'+err_amount+'</p>'+'<p>'+err_payThrough+'</p>');
                    }

                    $('.close_btn').click(function() {
                        location.reload();
                    });
                },
            });
            e.preventDefault(); //STOP default action
        });

//Payout Edit form

$(".payoutEditForm").submit(function(e)
{
    $('.submit_edit_payout').prop("disabled", true);
    $('.close_btn').hide();
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
        dataType: 'JSON',
        data : postData,
        success:function(data)
        {
            if(typeof(data.success) != 'undefined')
            {
                $('.close_btn').show();
                $('.alert_error').hide();
                $('.alert_success').removeClass('hide');
                $('.submit_edit_email').prop("disabled", false);
            }
            else
                {
                    $('.submit_edit_payout').prop("disabled", false);
                    $('.close_btn').show();

                    $('.alert_error').removeClass('hide');
                    err_amount = (typeof(data.amount) != 'undefined') ? data.amount : "";
                    $('.valid_err').html('<p>'+err_amount+'</p>');
                }

                $('.close_btn').click(function() {
                    location.reload();
                });
            },
        });
        e.preventDefault(); //STOP default action
        // e.unbind('submit'); //unbind. to stop multiple form submit.
    });
//End Payout Edit Form VALIDATION

$('.trackingCodeBtn').click(function() {
    $('.trackingCodeScript').empty();
    var code = $(this).attr('data-product-code');
    var label = $(this).attr('data-product-name');
    var trackingScript = "<textarea rows='6' class='form-control trackingCodeArea' onclick='this.focus();this.select()' readonly='readonly'><script type='text/javascript'>var _dkas = _dkas || [];_dkas.push(['"+code+"']);(function() {var dkas = document.createElement('script'); dkas.type = 'text/javascript'; dkas.async = true;dkas.src =  'http://affiliates.paykickstart.com/assets/js/tracker.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(dkas, s);})();</script></textarea>";
    $('.trackingCodeScript').append(trackingScript);
    $('.trackingCodeLabel').html(label+" Tracking Code Script");
});

    /* Select All text */
    $(".selectAll").click(function () {
        $(this).select();
    });

    /**
    *   Return trucated string (url, text, numbers...)
    */
    function truncate(text) {
        var size = text.length;
        if(size <= 100){size = size / 2;}else{size = size / 4;}
        var trunc_text = jQuery.trim(text).substring(0,size)+"...";
        return trunc_text;
    }

    function resetForm($form) {
        $form.find('input:text, input:password, input:file, select, textarea').val('');
        $form.find('input:radio, input:checkbox')
        .removeAttr('checked').removeAttr('selected');
    }

    function close_window()
    {
        close();
    }

    function promoMats () {
        var id = $('#prompProducts').val();
        if(typeof id != 'undefined')
        {
            window.location.replace(BASE_URL+"/promo-materials/detail/"+id);
        }
    }


    /**
    * Get plans via product id
    * in search filters
    */
    $(function(){

        $("#product_id").change(function(e){

            var product_id = $(e.currentTarget).val();
            var plan_menu = $('#plan_id');

        // Clear all old values
        plan_menu.empty();

        // Show loading
        plan_menu.append($('<option>').text('Loading...').attr('value', ''));

        if(product_id != "")
        {
            $.ajax({
                type: "GET",
                url: BASE_URL + '/get-plans/' + product_id,
                data: '',
                dataType: 'json',
                success: function(plans)
                {
            // Clear all old values
            plan_menu.empty();

            // Add first option
            plan_menu.append($('<option>').text('Select').attr('value', ''));

            // Loop all other values
            $.each(plans, function( index, value )
            {
                plan_menu.append($('<option>').text(value.plan_name).attr('value', value.id));
            });

          }
        });
        }
        else{
            plan_menu.empty();
        }
    });
    });



var serviceName;
var api_key
$('.connect-btn').click(function(event) {

	serviceName = $(this).attr('data-service');

	$('.modal-header').html('Connect '+serviceName);

	if(serviceName == 'GoToWebinar')
	{
		$('.service-label').html('GoToWebinar URL');
	}
	else{$('.service-label').html('API Key');}

	if(serviceName == 'GetResponse' || serviceName == 'MailChimp')
	{
		$('#serviceModal').modal();
	}

	if(  serviceName == 'SendReach' )
	{
		$('.service-label[for="user_id"]').html('User-ID')
		$('.service-label[for="api_key"]').html('API key')
		$('.service-label[for="api_secret_key"]').html('Secret key')
		$('#serviceModal-sendreach').modal();
	}

	if(  serviceName == 'iContact')
	{
		$('.service-label[for="user_id"]').html('APPID')
		$('.service-label[for="api_key"]').html('API USERNAME')
		$('.service-label[for="api_secret_key"]').html('API PASSWORD')
		$('#serviceModal-sendreach').modal();
	}

	if(  serviceName == 'InfusionSoft')
	{
		$('.service-label[for="user_id"]').html('User-ID')
		$('.service-label[for="api_key"]').html('API key')
		$('#serviceModal-infusion').modal();
	}

    if(  serviceName == 'ConstantContact')
    {
        $('.service-label[for="access_token"]').html('Access Token')
        $('.service-label[for="api_key"]').html('API key')
        $('#serviceModal-constantContact').modal();
    }
    if(  serviceName == 'ActiveCampaign')
    {
        $('.service-label[for="api_url"]').html('API URL');
        $('.service-label[for="api_key"]').html('API key');
        $('#serviceModal-activeCampaign').modal();
    }
    if(  serviceName == 'Ontraport')
    {
        $('.service-label[for="app_id"]').html('APP ID');
        $('.service-label[for="api_key"]').html('API key');
        $('#serviceModal-ontraport').modal();
    }
    if(  serviceName == 'Interspire')
    {
        $('.service-label[for="app_username"]').html('APP USERNAME');
        $('.service-label[for="app_usertoken"]').html('APP USERTOKEN');
        $('.service-label[for="api_path"]').html('API PATH');
        $('#serviceModal-InterspireHelper').modal();
    }

	if(serviceName == 'Aweber')
	{
		window.location.replace(BASE_URL+"/admin/connectAweber");
		return;
	}

});

$('#api_key').keypress(function(e){
    if ( e.which == 13 ) return false;
    //or...
    if ( e.which == 13 ) e.preventDefault();
});

$("#serviceConnectSubmit").click(function(e)
    {
        api_key = $('#api_key').val();
        var postData = 'service_name='+serviceName+'&value='+api_key;
        var formURL = BASE_URL+'/admin/connectAPI';

        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(data.status == '1')
                {
                    $('#serviceConnectSubmit').val('Connected');
                    $('#serviceConnectSubmit').removeClass('btn-primary');
                    $('#serviceConnectSubmit').addClass('btn-success');
                    setTimeout(
                        function()
                        {
                            $('#serviceModal').modal('hide');
                        }, 2000);
                    window.location.replace(BASE_URL+"/admin/integrations#setting-2");
                }
            }
        })
    });



$("#serviceConnectSubmit-sendreach").click(function(e)
	{
		apiKey = $('#sr_api_key').val();
		userId = $('#sr_user_id').val();
		apiSecretKey = $('#sr_api_secret_key').val();
		var postData = {
			api_key : apiKey,
			user_id : userId,
			api_secret_key : apiSecretKey,
			service_name : serviceName
		}
		var formURL = BASE_URL+'/admin/connectAPI';

		$.ajax(
		{
			url : formURL,
			type: "POST",
			dataType: 'JSON',
			data : postData,
			success:function(data)
			{
				if(data.status == '1')
				{
					$('#serviceConnectSubmit').val('Connected');
					$('#serviceConnectSubmit').removeClass('btn-primary');
					$('#serviceConnectSubmit').addClass('btn-success');
					setTimeout(
						function()
						{
							$('#serviceModal').modal('hide');
						}, 2000);
					window.location.replace(BASE_URL+"/admin/integrations");
				}
                if( data.status == '-1' )
                {
                    $('#serviceModalAlert-sendreach').append( data.message );
                    $('#serviceModalAlert-sendreach').fadeIn();
                }
			}
		})
	});


$("#serviceConnectSubmit-infusion").click(function(e)
	{
		apiKey = $('#inf_api_key').val();
		userId = $('#inf_user_id').val();
		var postData = {
			api_key : apiKey,
			user_id : userId,
			service_name : serviceName
		}
		var formURL = BASE_URL+'/admin/connectAPI';

		$.ajax(
		{
			url : formURL,
			type: "POST",
			dataType: 'JSON',
			data : postData,
			success:function(data)
			{
				if(data.status == '1')
				{
					$('#serviceConnectSubmit').val('Connected');
					$('#serviceConnectSubmit').removeClass('btn-primary');
					$('#serviceConnectSubmit').addClass('btn-success');
					setTimeout(
						function()
						{
							$('#serviceModal').modal('hide');
						}, 2000);
					window.location.replace(BASE_URL+"/admin/integrations");
				}
                if( data.status == '-1' )
                {
                    $('#serviceModalAlert-infusion').append( data.message );
                    $('#serviceModalAlert-infusion').fadeIn();
                }
			}
		})
	});

$("#serviceConnectSubmit-cc").click(function(e)
    {

        accessToken = $('#cc_access_token').val();
        apiKey = $('#cc_api_key').val();
        var postData = {
            cc_access_token : accessToken,
            cc_api_key : apiKey,
            service_name : serviceName
        }
        var formURL = BASE_URL+'/admin/connectAPI';

        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(data.status == '1')
                {
                    $('#serviceConnectSubmit').val('Connected');
                    $('#serviceConnectSubmit').removeClass('btn-primary');
                    $('#serviceConnectSubmit').addClass('btn-success');
                    setTimeout(
                        function()
                        {
                            $('#serviceModal').modal('hide');
                        }, 2000);
                    window.location.replace(BASE_URL+"/admin/integrations");
                }
                if( data.status == '-1' )
                {
                    $('#serviceModalAlert-cc').append( data.message[0].error_message );
                    $('#serviceModalAlert-cc').fadeIn();
                }
            }
        })
    });

$("#serviceConnectSubmit-activeCampaign").click(function(e)
    {

        apiUrl = $('#ac_api_url').val();
        apiKey = $('#ac_api_key').val();
        var postData = {
            ac_api_url : apiUrl,
            ac_api_key : apiKey,
            service_name : serviceName
        }
        var formURL = BASE_URL+'/admin/connectAPI';

        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(data.status == '1')
                {
                    $('#serviceConnectSubmit').val('Connected');
                    $('#serviceConnectSubmit').removeClass('btn-primary');
                    $('#serviceConnectSubmit').addClass('btn-success');
                    setTimeout(
                        function()
                        {
                            $('#serviceModal').modal('hide');
                        }, 2000);
                    window.location.replace(BASE_URL+"/admin/integrations");
                }
                if( data.status == '-1' )
                {
                    $('#serviceModalAlert-activeCampaign').append( data.message );
                    $('#serviceModalAlert-activeCampaign').fadeIn();
                }
            }
        })
    });

$("#serviceConnectSubmit-ontraport").click(function(e)
    {

        var appID = $('#ontr_app_id').val();
        var apiKey = $('#ontr_api_key').val();
        var postData = {
            ontr_app_id : appID,
            ontr_api_key : apiKey,
            service_name : serviceName
        }
        var formURL = BASE_URL+'/admin/connectAPI';

        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(data.status == '1')
                {
                    $('#serviceConnectSubmit').val('Connected');
                    $('#serviceConnectSubmit').removeClass('btn-primary');
                    $('#serviceConnectSubmit').addClass('btn-success');
                    setTimeout(
                        function()
                        {
                            $('#serviceModal').modal('hide');
                        }, 2000);
                    window.location.replace(BASE_URL+"/admin/integrations");
                }
                if( data.status == '-1' )
                {
                    $('#serviceModalAlert-ontraport').append( data.message );
                    $('#serviceModalAlert-ontraport').fadeIn();
                }
            }
        })
    });

$("#serviceConnectSubmit-InterspireHelper").click(function(e)
    {

        var appUsername = $('#intr_username').val();
        var apiUsertoken = $('#intr_usertoken').val();
        var apiPath = $('#intr_api_path').val();
        var postData = {
            intr_username : appUsername,
            intr_usertoken : apiUsertoken,
            intr_api_path : apiPath,
            service_name : serviceName
        }
        var formURL = BASE_URL+'/admin/connectAPI';

        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(data.status == '1')
                {
                    $('#serviceConnectSubmit').val('Connected');
                    $('#serviceConnectSubmit').removeClass('btn-primary');
                    $('#serviceConnectSubmit').addClass('btn-success');
                    setTimeout(
                        function()
                        {
                            $('#serviceModal').modal('hide');
                        }, 2000);
                    window.location.replace(BASE_URL+"/admin/integrations");
                }
                if( data.status == '-1' )
                {
                    $('#serviceModalAlert-InterspireHelper').append( data.message );
                    $('#serviceModalAlert-InterspireHelper').fadeIn();
                }
            }
        })
    });


    $(".disconnect-btn").click(function(event)
    {
        serviceName = $(this).attr('data-service');

        var postData = 'service_name='+serviceName;
        var formURL = BASE_URL+'/admin/disconnectAPI';

        $.ajax(
        {
            url : formURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                // console.log(data);
                if(data.status == '1')
                {
                    window.location.replace(BASE_URL+"/admin/integrations#setting-2");
                }
            }
        })
    });

$(document).ready(function($) {
    $('#add_blacklist_user').prop('disabled', false);
    $('#blacklist_email').val('');
    $('#add_blacklistip_user').prop('disabled', false);
    $('#blacklist_ip').val('');
});

$('#add_blacklist_user').click(function(event)

	{

		$(this).prop('disabled', true);
		var blacklist_email = $('#blacklist_email').val();
		var postData = 'email='+blacklist_email;
		postURL = BASE_URL+'/admin/blacklist/add';

		$.ajax(
		{
			url : postURL,
			type: "POST",
			dataType: 'JSON',
			data : postData,
			success:function(data)
			{
				if(data.status == '1')
				{
					$('#blacklist_email').val('');
					$('#blacklist_table > tbody:last').prepend('<tr><td class="pad-left" style="padding-left:200px !important;">'+blacklist_email+'</td><td class="pad-left"><a href="'+BASE_URL+'/admin/blacklist/remove/'+blacklist_email+'" class="btn btn-danger btn-xs">Remove</a></td></tr>');
					$('.alerts').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>Email Added</div>');
					$( ".alert" ).fadeOut( 3000, function() {
						$('#add_blacklist_user').prop('disabled', false);

					});
				}
				if(data.status == '-1' || typeof(data.email) != 'undefined')
				{
					err_message = (typeof(data.email) != 'undefined') ? data.email : "Email already exist.";
					$('.alerts').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>'+err_message+'</div>');
					$( ".alert" ).fadeOut( 3000, function() {
						$('#add_blacklist_user').prop('disabled', false);

					});
				}
			}
		})
	});

$('#add_blacklistip_user').click(function(event)

    {

        $(this).prop('disabled', true);
        var blacklist_ip = $('#blacklist_ip').val();
        var postData = 'ip_address='+blacklist_ip;
        postURL = BASE_URL+'/admin/blacklistip/add';

        $.ajax(
        {
            url : postURL,
            type: "POST",
            dataType: 'JSON',
            data : postData,
            success:function(data)
            {
                if(data.status == '1')
                {
                    $('#blacklist_ip').val('');
                    $('#blacklist_table > tbody:last').prepend('<tr><td class="pad-left" style="padding-left:200px !important;">'+blacklist_ip+'</td><td class="pad-left"><a href="'+BASE_URL+'/admin/blacklistip/remove/'+blacklist_ip+'" class="btn btn-danger btn-xs">Remove</a></td></tr>');
                    $('.alerts').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>Ip Added</div>');
                    $( ".alert" ).fadeOut( 3000, function() {
                        $('#add_blacklistip_user').prop('disabled', false);

                    });
                }
                if(data.status == '-1' || typeof(data.ip_address) != 'undefined')
                {
                    err_message = (typeof(data.ip_address) != 'undefined') ? data.ip_address : "IP already exist.";
                    $('.alerts').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>'+err_message+'</div>');
                    $( ".alert" ).fadeOut( 3000, function() {
                        $('#add_blacklistip_user').prop('disabled', false);

                    });
                }
            }
        })
    });

    function splitARCode() {
        var tags = ['a','iframe','frame','frameset','script'], reg, val = $('#form_code').val(),
        hdn = $('#arcode_hdn_div2'), formurl = $('#ar_other_action'), hiddenfields = $('#ar_other_hidden');
        formurl.val('');
        if($.trim(val) == '')
        {
            $('#ar_other_name').val('');
            $('#ar_other_email').val('');
            $('#ar_other_hidden').val('');
            $('#ar_hidden_str').val('');
            return false;
        }
        $('#arcode_hdn_div').html('');
        $('#arcode_hdn_div2').html('');

        /*mailchimp sometimes gives type="email"*/
        val = val.replace('type="email"','type="text"');

        for(var i=0;i<5;i++)
        {
            reg = new RegExp('<'+tags[i]+'([^<>+]*[^\/])>.*?</'+tags[i]+'>', "gi");
            val = val.replace(reg,'');

            reg = new RegExp('<'+tags[i]+'([^<>+]*)>', "gi");
            val = val.replace(reg,'');
        }

        var tmpval;
        try {
            tmpval = decodeURIComponent(val);
        } catch(err){
            tmpval = val;
        }

        hdn.append(tmpval);
        var num = 0;
        var name_selected = '';
        var email_selected = '';

        $(':text',hdn).each(function(){
            var name = $(this).attr('name'),

            name_selected = num == '0' ? name : (num != '0' ? name_selected : ''),
            email_selected = num == '1' ? name : email_selected;
                        /*if(name_selected && name_selected.indexOf('email') != -1)
                        alert(name_selected);*/
                        if(num=='0')
                        {
                            if(name_selected && name_selected.toLowerCase().indexOf('email') != -1)
                                $('#ar_other_email').val(name_selected);
                            else
                                $('#ar_other_name').val(name_selected);
                        }

                        if(num=='1')
                        {
                            if(email_selected && email_selected.toLowerCase().indexOf('name') != -1)
                                $('#ar_other_name').val(email_selected);
                            else
                                $('#ar_other_email').val(email_selected);
                        }

                        num++;
                    });

        var ar_hidden_str = '';
        $(':input[type=hidden]',hdn).each(function(){
            ar_hidden_str += (ar_hidden_str == '' ? '' : ',')+$(this).attr('name')+'='+$(this).val();
            $('#arcode_hdn_div').append($('<input type="hidden" name="'+$(this).attr('name')+'" />').val($(this).val()));
        });
        var hidden_f = $('#arcode_hdn_div').html();
        formurl.val($('form',hdn).attr('action'));
        hiddenfields.val(hidden_f);

        $('#ar_hidden_str').val(ar_hidden_str);
      //alert(tmpval);
      hdn.html('');
  }


  $('.switchStatus').on('toggle', function (e, active) {
        var product_id = $(this).data('product-id');
        var notification_cat = $(this).data('notification-cat');

      $.ajax({

            type: "POST",
            url: BASE_URL + '/admin/product-notification/change_status',
            data: {product_id : product_id, notification_cat: notification_cat },
            dataType: "json",
            success: function(dataJSON){
                // console.log("working");
            }
        });
    });

    $('.switchNotificationStatus').on('toggle', function (e, active) {
        var notification_type = $(this).data('notification-type')

      $.ajax({

            type: "POST",
            url: BASE_URL + '/admin/notification/change_status',
            data: {notification_type : notification_type },
            dataType: "json",
            success: function(dataJSON){
                // console.log(dataJSON);
            }
        });
    });
