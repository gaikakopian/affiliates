<?php

	namespace App\Http\Controllers\Affiliate;

	use App\Events\UpdateAffiliatePromoteEvent;
	use App\Helpers\AffiliateApproval;
    use App\Helpers\AffiliatesHelper;
    use App\Helpers\ExportHelper;
	use App\Helpers\GeneralHelper;
	use App\Helpers\Permissions;
    use App\Helpers\PKLog;
    use App\Http\Controllers\Controller;
	use App\Http\Requests\AddCampaignRequest;
	use App\Http\Requests\FilterCommissionsRequest;
	use App\Models\AffiliateRequest;
	use App\Models\AffiliateVendor;
    use App\Models\Commission;
    use App\Models\EmailInstruction;
	use App\Models\Funnel;
	use App\Models\FunnelPlan;
	use App\Models\OverRideCommission;
	use App\Models\Pkconfig;
	use App\Models\Plan;
	use App\Models\Product;
	use App\Models\ProductPermissions;
	use App\Models\Purchase;
	use App\Services\AffiliateService;
	use App\User;
	use Auth;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Input;
	use Session;

	class AffiliatesController extends Controller {

		/**
		 * Properties
		 */
		protected $_data;

		/**
		 * Constructor
		 */
		function __construct() {
			parent::__construct();

			$this->_data = array();

			$this->_data[ 'section' ] = "affiliates";
		}

//		/* Get Affiliate Login Page */
//		public function getLogin() {
//			return view( 'affiliate.auth.login' );
//		}

		public function dashboard() {
			$pkconfig = Pkconfig::where( 'user_id', \Auth::user()->id )->get();

			return view( 'affiliate.dashboard' )->with( 'pkconfig', $pkconfig );
		}

		/**
		 * List all Affiliates
		 */
		public function index() {
			return view( 'admin.affiliates.index' )->with( AffiliatesHelper::getAffiliates( $this->processQueryData()) );
		}

		public function getCommissions() {
			$product_id   = request()->product_id;
			$affiliate_id = request()->u_id;

			$vendor     = auth()->user()->id;
			$user       = User::with( 'campaigns', 'campaigns.plans', 'campaigns.plans.commissions' )->find( $vendor );
			$commisions = array();
			foreach( $user->campaigns()->where( 'id', $product_id )->get() as $product ) {
				foreach( $product->plans()->get() as $plan ) {
					foreach( $plan->commissions()->where( 'affiliate_id', $affiliate_id )->get() as $com ) {
						$com[ 'product_name' ]  = $product->name;
						$com[ 'campaign_name' ] = $plan->name;
						$com[ 'campaign_id' ]   = $plan->id;
						if( !is_null( $com->payout_id ) ) {
							$com[ 'pending' ] = "";
							$com[ 'paid' ]    = $com->amount;
						} else {
							$com[ 'pending' ] = $com->amount;
							$com[ 'paid' ]    = "";
						}
						$commisions[] = $com;
					}
				}
			}

			$data[ 'commisions' ]   = $commisions;
			$data[ 'affiliate_id' ] = $affiliate_id;
			$data[ 'product_id' ]   = $product_id;
			$data[ 'affiliate' ]    = User::find( $affiliate_id )->first_name.' '.User::find( $affiliate_id )->last_name;

			return view( 'admin.affiliates.ajax.index', $data );
		}

		/**
		 * Return all affiliates for mobile
		 *
		 * @return mixed
		 */
		public function getAllAffiliates( $user_id ) {
			$filterMap = [
				'tax_form' => 'users.tax_form',
				'status'   => 'users.status',
				'payout'   => 'product_permissions.promote'
			];

			$search = [
				'select' => [
					// affiliates
					'users.id',
					'users.first_name',
					'users.last_name',
					'users.status',
					'users.email',
					'users.paypal_email',
					'users.tax_form',
					'users.tax_form_type',
					'product_permissions.id as permission_id',
					'product_permissions.product_id as product_id',
					'product_permissions.promote'
				],

				'columns' => [],

				'joins' => [
					'product_permissions' => [ 'users.id', 'product_permissions.user_id' ]
				]
			];

			$data  = $this->processQueryData();
			$query = User::search( $data[ 'searchQuery' ], $search )
			             ->where( 'users.id', '!=', $user_id )
			             ->where( 'product_permissions.promote', '!=', 0 );

			if( isset( $data[ 'filters' ][ 'tax_form' ] ) ) {
				$tax_form = $data[ 'filters' ][ 'tax_form' ];

				if( $tax_form == '0' ) {
					$data[ 'filters' ][ 'tax_form' ] = null;
				} elseif( $tax_form == '1' ) {
					unset( $data[ 'filters' ][ 'tax_form' ] );
					$query->whereNotNull( 'tax_form' );
				}
			}

			$query->distinct()
			      ->permissions( 'affiliates', 'product_permissions', 'product_id' );

			$data[ 'affiliates' ] = $query->filter( $filterMap, $data[ 'filters' ] )->orderBy( 'users.created_at', 'DESC' )->get();

			return $data[ 'affiliates' ];
		}

		// ------------------------------------------------------------------------
		public function getAffiliateCampaigns( $affiliate_id ) {
			$affiliate = User::find( $affiliate_id );

			return view( 'admin.affiliates.campaigns', compact( 'affiliate' ) );
		}

		/**
		 * @param $affiliate_id
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function getAffiliatePayoutDetails( $affiliate_id, AffiliateService $affiliate, $product_id = null ) {
			//Get filters from url
			$filters = $this->getFilters( $_GET );

			//Date range
			$date = GeneralHelper::getDateRange();

			//product id from filter
			$filtered_product_id = null;

			//If product_id is in filter so override original product_id
			if( !empty( $filters[ 'product_id' ] ) ) {
				$filtered_product_id = $filters[ 'product_id' ];
			}

			//If data range was chosen in filter so we overide $date array
			if( !empty( $filters[ 'startDate' ] ) && !empty( $filters[ 'endDate' ] ) ) {
				$date[ 'startDate' ] = $filters[ 'startDate' ];
				$date[ 'endDate' ]   = $filters[ 'endDate' ];
			}

			$data = $affiliate->getAffiliatePayoutdetails( $affiliate_id, $product_id, $date, $filtered_product_id );

			$commissions = $data[ 'commissions' ];
			$full_list   = $data[ 'full_list' ];

			$this->_data[ 'commissions' ] = $commissions;

			$coll_products = collect( $full_list );

			$this->_data[ 'products' ] = $coll_products->unique( 'product_id' );

			//List of sorted products list
			$products_list = [];

			//Make array with product_id => product_name
			foreach( $this->_data[ 'products' ] as $product ) {
				$products_list[ $product->product_id ] = $product->product_name;
			}

			//Sort products list by value
			asort( $products_list, SORT_NATURAL | SORT_FLAG_CASE );

			$this->_data[ 'products' ] = $products_list;

			$range                      = GeneralHelper::getDateRange();
			$this->_data[ 'startDate' ] = Carbon::parse( $range[ 'startDate' ] )->format( 'm/d/Y' );
			$this->_data[ 'endDate' ]   = Carbon::parse( $range[ 'endDate' ] )->format( 'm/d/Y' );

			return view( 'admin.payouts.details' )->with( $this->_data );
		}

		/**
		 * @param $affiliate_id
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function getFilterAffiliatePayoutDetails( $affiliate_id, FilterCommissionsRequest $request, AffiliateService $affiliate ) {
			$product     = $request->by_product;
			$commissions = $affiliate->getTransactions( $affiliate_id, $product, $request->data );

			foreach( $commissions as $v ) {
				if( is_null( $v->payout_id ) ) {
					$v->pending = $v->amount;
					$v->paid    = '';
				} else {
					$v->paid    = $v->amount;
					$v->pending = '';
				}
				$dif_days        = ( strtotime( "now" ) - ( strtotime( $v->created_at ) + $v->ref_days ) ) / ( 60 * 60 * 24 );
				$v->days_pending = (int) $dif_days;
				$v->created_at   = \App\Helpers\TimeHelper::get_current_timezone_time( $v->created_at, \App\Models\PlatformSetting::getTimeZone( Auth::id() ) );
			}
			$this->_data[ 'commissions' ] = $commissions;
			//$product_list=Commission::getCommissionsProducts($affiliate_id);
			$product_list = $affiliate->getAffiliatePayoutdetails( $affiliate_id, $product );

			$coll_products             = collect( $product_list );
			$this->_data[ 'products' ] = $coll_products->unique( 'product_name' );

			return view( 'admin.payouts.details' )->with( $this->_data );
		}

		/**
		 * @param Request $request
		 *
		 * @return mixed
		 */
		public function getAffiliateList() {
			$filterMap = [
					'status'       => 'users.status',
					'affiliate_id' => 'users.id'
			];

			$search = [
					'select' => [
						// affiliates
						'users.id',
						'users.first_name',
						'users.last_name',
						'users.`status` as status',
						'users.email',
						'users.image',
						'users.paypal_email',
						'users.tax_form',
						'users.tax_form_type',
						'com.totalPaid as totalPaid',
						'com.totalPending as totalPending',
					    'product_permissions.promote as promote'
					],

					'columns' => [
							'users.first_name' => 0,
							'users.last_name'  => 0,
							'users.email'      => 0,
					],

					'joins' => [
							'product_permissions' => [ 'users.id', 'product_permissions.user_id' ]
					]
			];

			$data = $this->processQueryData();

			// Get all product owners I have affiliate read rights to
			$owner_products = Permissions::getAllowedProducts( 'affiliates', 1 );
			$owners         = Permissions::getOwnersByProducts( $owner_products );

			if (empty($owner_products) || empty($owners)) {
			    return [];
            }

			// Get all all affiliates for each of those owners
			$affiliates = AffiliateRequest::select( DB::raw( "products.user_id AS owner_id" ), "affiliates_requests.affiliate_id AS user_id", "affiliates_requests.status", "affiliates_requests.product_id" )
					->leftjoin( "products", "products.id", "=", "affiliates_requests.product_id" )
					->whereNotNull( "affiliates_requests.status" )
					->whereIn( "affiliates_requests.product_id", $owner_products )
					->groupBy( "affiliates_requests.affiliate_id" )
					->unionAll( ProductPermissions::select( "owner_id", "user_id", DB::raw( "1 AS status" ), "product_id" )
							->whereIn( "owner_id", $owners )
							->whereRaw( "owner_id != user_id" )
							->where( "promote", ">=", 1 )
							->groupBy( "user_id" ) )
					->get();

			$affiliate_ids = $affiliates->pluck( "user_id" )->toArray();

			// Get all the stats
			$query = User::search( $data[ 'searchQuery' ], $search )
					->whereIn( "users.id", $affiliate_ids )
					->leftJoin(DB::raw("
									(SELECT commissions.affiliate_id AS id,
								 		SUM(IF((commissions.status = 2 AND ( transactions.is_refund = 0 OR ( transactions.is_refund = 1 AND commissions.is_manual_payout = 1 AND commissions.payout_id IS NOT NULL)))
									 		OR(commissions.status != 2 AND transactions.is_refund = 0), commissions.amount, 0)) as totalPaid,
									 	SUM(IF(commissions.`status` != 2 AND transactions.is_refund = 0, commissions.amount, 0)) as totalPending
									FROM commissions
							    	JOIN transactions ON transactions.id = commissions.transaction_id
							    	JOIN purchases ON purchases.id = transactions.purchase_id
							    	WHERE purchases.product_id IN (". join(',',$owner_products) .")
							    	GROUP BY commissions.affiliate_id) as com"), 'com.id', '=', 'users.id'
					)
					->groupBy( "users.id" );

			$query->groupBy( 'users.id' );
			$query->filter( $filterMap, $data[ 'filters' ] );

			// By default is commission
			$sort_by = isset($data[ 'filters' ]['sort_by']) ? $data[ 'filters' ]['sort_by'] : 'commission';

			switch ($sort_by) {
				case 'alphabetical':
					$query->orderBy( DB::raw('CONCAT(users.first_name, " ", users.last_name)'), 'ASC' );
					break;
				case 'signup':
					$query->orderBy( 'users.created_at', 'DESC' );
					break;
				case 'commission':
					$query->orderBy( 'totalPaid', 'DESC' );
					break;
			}

			return [
                'queryBuilder' => $query,
                'allowsProductIds' => $owner_products
            ];
		}

        /**
         * @return \Illuminate\Http\RedirectResponse
         */
		public function exportAffiliateList() {
            $query = $this->getAffiliateList();

            if (Input::get('force_page')) {
                $affiliatesList = $query['queryBuilder']->paginate(Input::get( 'items_per_page' ) ?: 50);
            } else {
                $affiliatesList = $query['queryBuilder']->get();
            }

			$affiliates = [
				[
				    'ID',
					'First Name',
					'Last Name',
					'Email',
					'Status',
					'Paid Commissions',
					'Paypal Email',
					'Payout Type'
				],
			];

			foreach( $affiliatesList as $affiliate ) {
				$affiliates[] = [
                    $affiliate->id,
					$affiliate->first_name,
					$affiliate->last_name,
					$affiliate->email,
					( $affiliate->status == 1 ) ? 'Active' : ( ( $affiliate->status == 0 ) ? 'Suspended' : ( ( $affiliate->status == 2 ) ? 'Unapproved' : '' ) ),
					'$'.number_format($affiliate->totalPaid, 2),
					$affiliate->paypal_email,
					( $affiliate->promote == 1 ) ? 'Delayed' : ( $affiliate->promote == 2 ? 'Instant' : '' )
				];
			}

			ExportHelper::exportCSVFromList( 'Affiliates\' List', 'Affiliates', $affiliates );
		}

		/**
		 * Affiliate Details
		 */
		public function getDetails( $affiliate_id ) {
			// Delete affiliate from campaign
			if( Input::get( 'action' ) == 'delete' && Input::get( 'campaign_id' ) ) {
				ProductPermissions::deleteAffiliateFromCampaign( $affiliate_id, Input::get( 'campaign_id' ) );

				//Set request status = 0 in affiliate_requests table
				AffiliateRequest::where( 'affiliate_id', $affiliate_id )->where( 'product_id', Input::get( 'campaign_id' ) )->update( [ 'status' => 0 ] );

				User::where( 'id', $affiliate_id )->update( [ 'refresh_permissions' => 1 ] );

//            return response()->json(['result' => 'success']);
				return redirect()->back()->with( 'alert_message', 'Affiliate has been deleted from selected campaign' );
			}

			if( Input::get( 'funnel_id' ) && Input::get( 'campaign_id' ) ) {
				$funnel_data = $this->funnelDetails( Input::get( 'funnel_id' ) );
				if( !empty( $funnel_data ) ) {
					$this->_data[ 'levels' ]              = $funnel_data[ 'levels' ];
					$this->_data[ 'current_funnel' ]      = $funnel_data[ 'current_funnel' ];
					$this->_data[ 'fe_plans' ]            = $funnel_data[ 'fe_plans' ];
					$this->_data[ 'plans' ]               = $funnel_data[ 'plans' ];
					$this->_data[ 'selectedFunnelPlans' ] = $funnel_data[ 'selectedFunnelPlans' ];
				}
			}

			$search = [
				// what to grab from query... this is to limit resources used
				'select'  => [
					// products info
					'products.name as product_name',
					'products.id as product_id',
					'products.dark_logo as product_logo',

					// affiliate info
					'users.id as user_id',
					'users.first_name',
					'users.last_name',
					'users.email',
					'product_permissions.id as permission_id'
				],

				// where to search for
				'columns' => [
					'product_permissions.user_id' => 1
				],

				'joins' => [
					'product_permissions' => [ 'users.id', 'product_permissions.user_id' ],
					'products'            => [ 'product_permissions.product_id', 'products.id' ],
					//                'plans' => [ 'product_permissions.product_id', 'plans.product_id' ],
				]
			];

			$this->_data[ 'products' ] = $products = User::search( $affiliate_id, $search )
			                                             ->where( 'product_permissions.promote', '!=', 0 )
			                                             ->distinct()
			                                             ->permissions( 'affiliates', 'product_permissions', 'product_id' )
			                                             ->get();

			if( count( $this->_data[ 'products' ] ) == 0 ) {
				// no products to show, grab info from users table
				$this->_data[ 'affiliate' ] = User::where( 'id', $affiliate_id )->first()->toArray();
			} else {
				// grab affiliate info from first row.
				$this->_data[ 'affiliate' ] = $affiliate = [
					'id'         => $this->_data[ 'products' ][ 0 ][ 'user_id' ],
					'first_name' => $this->_data[ 'products' ][ 0 ][ 'first_name' ],
					'last_name'  => $this->_data[ 'products' ][ 0 ][ 'last_name' ],
				];
			}
			for( $i = 0; $i < count( $this->_data[ 'products' ] ); $i++ ) {
				// $user_id = $products->user_id;
				if( Permissions::canWrite( $this->_data[ 'products' ][ $i ][ 'product_id' ], 'affiliates' ) ) {
					$this->_data[ 'products' ][ $i ][ 'canEdit' ] = 1;
				} else {
					$this->_data[ 'products' ][ $i ][ 'canEdit' ] = 0;
				}

				// Get funnels for override
				$this->_data[ 'products' ][ $i ][ 'funnels' ] = Funnel::overrideDetails( $this->_data[ 'products' ][ $i ][ 'product_id' ], $affiliate_id );
			}

			// Get all products where affiliate has been added
			$affiliate_product_ids = [];

			if( count( $this->_data[ 'products' ] ) > 0 ) {
				foreach( $this->_data[ 'products' ] as $affiliate_product ) {
					$affiliate_product_ids[] = $affiliate_product->product_id;
				}
			}

			$vendor = auth()->user()->id;
			$user   = User::with( 'campaigns' )->find( $vendor );

			// Delete campaigns where affiliate has already been added
			if( count( $user->campaigns ) > 0 ) {
				foreach( $user->campaigns as $key => $user_campaign ) {
					if( in_array( $user_campaign->id, $affiliate_product_ids ) ) {
						unset( $user->campaigns[ $key ] );
					}
				}
			}

			$this->_data[ 'campaigns' ] = $user->campaigns;

			if( \Request::ajax() ) {

				$product_id          = \Request::get( 'product_id' );
				$affiliate_id        = \Request::get( 'affiliate_id' );
				$search_funnel_title = \Request::get( 'search_key' );

				$funnels = Funnel::overrideDetails( $product_id, $affiliate_id, $search_funnel_title );

				$data = [
					'product_id'   => $product_id,
					'affiliate_id' => $affiliate_id,
					'funnels'      => $funnels
				];

				return response()->json( view( 'admin.affiliates.modals.funnelDetailsPage', $data )->render() );
			}

			return view( 'admin.affiliates.details', $this->_data )->render();
		}

		/* Add campaign
		*
		*
		*/
		public function addCampaign( $aff_id, AddCampaignRequest $request ) {

			// Check if affiliate has not already been added
			if( ProductPermissions::where( 'user_id', $aff_id )->where( 'product_id', $request->campaign )->first() ) {
				return redirect()->back()->with( 'alert_error', 'Affiliate had already been added previously to promote selected campaign.' );
			}

			$p_permissions = ProductPermissions::insert( [
				                                             'user_id'       => $aff_id,
				                                             'product_id'    => $request->campaign,
				                                             'campaigns'     => 0,
				                                             'licensing'     => 0,
				                                             'transactions'  => 0,
				                                             'subscriptions' => 0,
				                                             'customers'     => 0,
				                                             'affiliates'    => 0,
				                                             'marketing'     => 0,
				                                             'payouts'       => 0,
				                                             'promote'       => 2,
				                                             'created_at'    => date( "Y-m-d H:i:s" ),
				                                             'reports'       => 0
			                                             ] );

			//Add row in affiliate_request table
			$newRequest               = new AffiliateRequest();
			$newRequest->affiliate_id = $aff_id;
			$newRequest->product_id   = $request->campaign;
			$newRequest->status       = 1;
			$newRequest->rid          = '';

			$newRequest->save();

			return redirect()->back()->with( 'alert_message', 'Affiliate has been added successfully to promote the selected campaign.' );
		}

		/**
		 *
		 */
		public function UpdateCommission() {
			if( !Permissions::canWrite( Input::get( 'product_id' ), 'affiliates' ) ) {
				return 'You don\'t have permissions to edit this.';
			}

			$affiliate_id = Input::get( 'affiliate_id' );
			$plan_id      = Input::get( 'plan_id' );

			// find the entry in over_ride_commissions
			$entry = OverRideCommission::where( 'affiliate_id', $affiliate_id )->where( 'plan_id', $plan_id )->first();

			if( is_null( $entry ) ) {
				OverRideCommission::create( [
					                            'affiliate_id' => $affiliate_id,
					                            'plan_id'      => $plan_id,
					                            'level1'       => Input::get( 'level1' ),
					                            'level2'       => Input::get( 'level2' ),
					                            'landing_url'  => Input::get( 'landing_url' ),
				                            ] );

				return 'ok';
			}

			$entry[ 'level1' ] = Input::get( 'level1' );
			$entry[ 'level2' ] = Input::get( 'level2' );

			$entry->update();

			return 'ok';
		}

		private function funnelDetails( $funnel_id ) {

			if( empty( $funnel_id ) ) {
				return [];
			}

			$funnel_data[ 'current_funnel' ] = $funnel = Funnel::single( $funnel_id );
			$funnel_data[ 'fe_plans' ]       = $fe_plans = Plan::funnelPlans( $funnel->product->user_id, $funnel_id, true );
			$funnel_data[ 'plans' ]          = $plans = Plan::funnelPlans( $funnel->product->user_id, $funnel_id );
			$funnel_data[ 'product_id' ]     = $funnel->product_id;

			$funnel_data[ 'selectedFunnelPlans' ] = FunnelPlan::selectedPlans( $funnel_id );

			$funnel_data[ 'levels' ] = FunnelPlan::getFunnelLevels( $funnel_id );

			// dd($funnel_data['levels']);

			return $funnel_data;
		}

		/**
		 * Remove or block affiliate
		 *
		 * @return array
		 */
		public function removeOrBlock()
		{
			$data = Input::all();

			// Return products with promote > 0 in product_permissions table
			$products = Permissions::getProductsByOwner( $data[ 'owner_id' ] );

			$return = [
				'success' => 0,
				'id'      => $data[ 'id' ]
			];

			switch( $data[ 'status' ] ) {
				case 'delete':
					AffiliateVendor::where( 'affiliate_id', $data[ 'u_id' ] )->where( 'user_id', $data[ 'owner_id' ] )->delete();
					ProductPermissions::where( 'user_id', $data[ 'u_id' ] )->whereIn( 'product_id', $products )->update( array( 'promote' => 0 ) );
					AffiliateRequest::where( 'affiliate_id', $data[ 'u_id' ] )->whereIn( 'product_id', $products )->delete();
					$return[ 'success' ] = 1;
					$return[ 'status' ]  = 'delete';
					break;
				case 'block':
					//don't allow automatic approve in future
//					AffiliateVendor::where( 'affiliate_id', $data[ 'u_id' ] )->where( 'user_id', $data[ 'owner_id' ] )->update( array( 'is_instant' => 0 ) );

					ProductPermissions::where( 'user_id', $data[ 'u_id' ] )
							->where( 'owner_id', $data[ 'owner_id' ] )
							->update([
									'promote' => DB::raw('promote * -1')
							]);

					AffiliateRequest::where( 'affiliate_id', $data[ 'u_id' ] )
							->whereIn( 'product_id', $products )
							->whereNotNull( 'status' )
							->update( array( 'status' => -1 ) );

                    Commission::whereAffiliateId($data[ 'u_id' ])
                        ->whereStatus('offline')
                        ->update([
                            'status' => Commission::STATUS_CANCELLED,
                            'deny_note' => 'Banned'
                        ]);

					$return[ 'success' ] = 1;
					$return[ 'status' ]  = 'block';
					break;
				case 'unblock':

					//we set affiliate request status = 0 (like in case of DENY), so affiliate after unbanned should send request again
					AffiliateRequest::where( 'affiliate_id', $data[ 'u_id' ] )->whereIn( 'product_id', $products )->where( 'status', -1 )->update( array( 'status' => 1 ) );

					//We reset affiliate permissions
					ProductPermissions::where( 'user_id', $data[ 'u_id' ] )
							->where( 'owner_id', $data[ 'owner_id' ] )
							->whereRaw('promote <= 0')
							->update([
									'promote' => DB::raw('promote * -1')
							]);

					$return[ 'success' ] = 1;
					$return[ 'status' ]  = 'approve';
					break;
			}

			Permissions::forceRefreshPermissions( $data[ 'u_id' ] );

			return $return;
		}

		public function settings( $affiliate_id )
		{
			$vendor = Auth::user();

			// Get all products that affiliate has access for
//			$affiliate_products = ProductPermissions::where('user_id', $affiliate_id)->where('promote', '>', 0)
//					->get()
//					->pluck('product_id')
//					->toArray();

			// Get all products that affiliate has permissions
			// Get products ID from $products_that_affiliate_has_permissions
			$prod_id_that_affiliate_has_permissions = AffiliateRequest::select([
					'affiliate_id',
					'affiliates_requests.product_id as p_id',
					DB::raw('product_permissions.*')
			])
					->leftJoin("product_permissions", function($query) use ($affiliate_id) {
						$query->on('product_permissions.product_id', '=', 'affiliates_requests.product_id');
						$query->where('product_permissions.user_id', '=', $affiliate_id);

					})
					->where('affiliates_requests.affiliate_id', $affiliate_id)
					->groupBy('affiliates_requests.product_id')
					->get()
					->pluck('p_id')
					->toArray();

			// Fetch owner_id of products from $affiliate_products that logged in user have 'affiliate' permission
			$owners_id = ProductPermissions::where('user_id', $vendor->id)
					->whereIn('product_id', $prod_id_that_affiliate_has_permissions)
					->where('affiliates', '>', 1)
					->groupBy('owner_id')
					->get()
					->pluck('owner_id')
					->toArray();

			// Get Owners info
			$owners_info = User::select(DB::raw('concat(users.first_name, " ", users.last_name) as name'), 'users.id')
					->whereIn('id', $owners_id)
					->orderBy('name')
					->pluck('name', 'id')
					->toArray();

			// AutoApprove settings
			$auto_approve_settings = AffiliateVendor::where('affiliate_id', $affiliate_id)
					->whereIn('user_id', $owners_id)
					->pluck('is_instant','user_id')
					->toArray();


			// Generating an array with auto approve data
			// 'owner_id' => ['name', 'is_instant']
			$owners_with_approve_data = [];
			foreach ($owners_info as $user_id => $name) {
				$owners_with_approve_data[$user_id] = [
						'name' => $name,
				        'is_instant' => isset($auto_approve_settings[$user_id]) ? $auto_approve_settings[$user_id] : null
					];
			}

			$affiliateSettings = DB::table( 'users' )
			                       ->select( '*' )
			                       ->where( 'users.id', $affiliate_id )
			                       //->join( 'platform_settings', 'users.id', '=', 'platform_settings.user_id' )
			                       ->first();

            // Get only allowed products that user can manage inside Affiliate section
			$allowed_products = Permissions::getAllowedProducts('affiliates');

            $products = Product::permissions ('campaigns')
                ->leftJoin ('product_permissions AS pp', function ($join) use ($affiliate_id) {
                    $join->on ('pp.product_id', '=', 'products.id');
                    $join->where ('pp.user_id', '=', $affiliate_id);
                })
                ->leftJoin ('affiliates_requests AS ar', function ($join) use ($affiliate_id) {
                    $join->on ('ar.product_id', '=', 'products.id');
                    $join->where ('ar.affiliate_id', '=', $affiliate_id);
                })
	            ->leftJoin ('users', 'users.id', '=', 'products.user_id')
	            ->where('products.affiliate_program', '!=', 0)
                ->whereIn('products.id', $allowed_products)
                ->select (
//                'ar.status as status',
                    'pp.promote',
                    'products.name as product_name',
                    'products.id as product_id',
                    'products.dark_logo as product_logo',
                    DB::raw('concat(users.first_name, " ", users.last_name) as vendor_name')
//                DB::raw('(SELECT status FROM affiliates_requests WHERE product_id = ar.product_id
//                         ORDER BY created_at DESC LIMIT 1) as status'
//                )
                )
                ->selectSub (function ($query) {
                    $query->select ('status')
                        ->from ('affiliates_requests')
                        ->where ('product_id', '=', DB::raw ('ar.product_id'))
                        ->where ('affiliate_id', '=', DB::raw ('ar.affiliate_id'))
                        ->orderBy ('status', 'desc')
                        ->limit (1);
                }, 'status')
                ->groupBy ('products.id')
                ->orderBy('products.name', 'asc')
                ->paginate (50);

			if( Input::get( 'funnel_id' ) && Input::get( 'campaign_id' ) ) {
				$funnel_data = $this->funnelDetails( Input::get( 'funnel_id' ) );
				if( !empty( $funnel_data ) ) {
					$this->_data[ 'levels' ]              = $funnel_data[ 'levels' ];
					$this->_data[ 'current_funnel' ]      = $funnel_data[ 'current_funnel' ];
					$this->_data[ 'fe_plans' ]            = $funnel_data[ 'fe_plans' ];
					$this->_data[ 'plans' ]               = $funnel_data[ 'plans' ];
					$this->_data[ 'selectedFunnelPlans' ] = $funnel_data[ 'selectedFunnelPlans' ];
				}
			}

			$productsQuantity = $products->count();
			$affiliate        = User::where( 'id', $affiliate_id )->first()->toArray();

			// Get all products where affiliate has been added
			$affiliate_product_ids = [];

			for( $i = 0; $i < $productsQuantity; $i++ ) {
				$product = $products[ $i ];

				if( Permissions::canWrite( $product->product_id, 'affiliates' ) ) {
					$product->canEdit = 1;
				} else {
					$product->canEdit = 0;
				}

				// Get funnels for override
				$product->funnels = Funnel::overrideDetails( $product->product_id, $affiliate_id );

				$affiliate_product_ids[] = $product->product_id;
			}

			// Delete campaigns where affiliate has already been added
			if( $vendor->campaigns->count() > 0 ) {
				foreach( $vendor->campaigns as $key => $user_campaign ) {

					if( in_array( $user_campaign->id, $affiliate_product_ids ) ) {
						unset( $vendor->campaigns[ $key ] );
					}
				}
			}

			// Products that logged in vendor has permission to
//			$products_that_vendor_has_permissions = ProductPermissions::where('user_id', $vendor->id)
//					->whereIn('product_id', $products_ID_that_affiliate_has_permissions)
//					->where('affiliates', '>', 1);

//			$products_id = ProductPermissions::select('product_id')->where('user_id', Auth::user()->id)
//					->where('affiliates', '>', 1)
//					->get()
//					->toArray();
//
//			$product_permissions = ProductPermissions::where( 'user_id', $affiliate_id )
//					->whereIn( 'product_id', $products_id )
//					->where('promote', '>', 0)
//					->get();

            // Get products by owner
            $products_by_owner = ProductPermissions::whereIn('owner_id', $owners_id)
                ->groupBy( "product_id")
                ->pluck( "product_id")
                ->toArray();

			// 1 - request was approved
			// 0 - request was denied
			// -1 - request was blocked
			// Get record from affiliate_request table
			$requests_count =  AffiliateRequest::where('affiliate_id', $affiliate_id)
					->whereIn('product_id', $products_by_owner)
					->where('status', '!=', -1)
					->whereNotNull('status')
					->count();

			// It there isn't any permission.promote > 0 OR there isn't any affiliate_request.status > -1
			// so affiliate is blocked for current vendor
			if ($requests_count == 0) {
				$is_blocked = 1;
			} else {
				$is_blocked = 0;
			}

//			$this->_data[ 'campaigns' ]           = $vendor->campaigns;
			$this->_data[ 'products' ]            = $products;
			$this->_data[ 'affiliate' ]           = $affiliate;
			$this->_data[ 'vendors' ]             = $owners_with_approve_data;
			$this->_data[ 'is_blocked' ]          = $is_blocked;
			$this->_data[ 'affiliateSettings' ]   = $affiliateSettings;
//			$this->_data[ 'product_permissions' ] = $prod_id_that_affiliate_has_permissions;

			return view( 'admin.affiliates.settings', $this->_data )->render();
		}

		public function getFunnelsByAffiliateID( Request $request ) {
			$campaignID  = $request->input( 'cid' );
			$affiliateID = $request->input( 'affid' );
			$searchKey   = $request->input( 'search' );

			$funnels = Funnel::overrideDetails( $campaignID, $affiliateID, $searchKey );
			$view    = view( 'admin.affiliates.modals.funnelDetails', [ 'funnels' => $funnels, 'affiliate_id' => $affiliateID, 'campaign_id' => $campaignID ] )->render();

			$response = [
				'status' => 'success',
				'html'   => $view,
			];

			return response()->json( $response );
		}

		public function commissionSettings() {
			$product_id = Input::get( 'productId' );

			if( Input::get( 'funnel_id' ) && Input::get( 'productId' ) ) {
				$funnel_data = $this->funnelDetails( Input::get( 'funnel_id' ) );
				if( !empty( $funnel_data ) ) {
					$this->_data[ 'levels' ]              = $funnel_data[ 'levels' ];
					$this->_data[ 'current_funnel' ]      = $funnel_data[ 'current_funnel' ];
					$this->_data[ 'fe_plans' ]            = $funnel_data[ 'fe_plans' ];
					$this->_data[ 'plans' ]               = $funnel_data[ 'plans' ];
					$this->_data[ 'selectedFunnelPlans' ] = $funnel_data[ 'selectedFunnelPlans' ];
				}
			}
			$products = DB::table( 'products' )
			              ->select( 'plans.*' )
			              ->where( 'products.id', $product_id )
			              ->join( 'plans', 'products.id', '=', 'plans.product_id' )
			              ->paginate( 1 );

			if( empty( $products ) ) {
				$success = "success";
				$view    = "<h3 style='text-align: center'>No products</h3>";
			} else {
				$data = [];
				foreach( $products as $plan ) {
					$data[] = [
						'id'           => $plan->id,
						'product_name' => $plan->name,
						'tiar1'        => $plan->level1,
						'tiar2'        => $plan->level2,

					];
				}

				$success = "success";
				$view    = view( 'admin.affiliates.modals.funnelDetails', [ 'data' => $data ] )->render();
			}

			return response()->json( [
				                         "success" => $success,
				                         "view"    => $view
			                         ] );
		}

		public function saveSettings( Request $request )
		{
			$vendor_id           = Auth::Id();
			$auto_approve        = $request->input( 'auto-approve' );
			$status              = $request->input( 'status' );
			$affiliate_id        = $request->input( 'affiliate_id' );
			$campaign_accesses   = $request->input( 'access' );

			// Current affiliate's vendor
			$request_approve_data  = $request->input( 'vendors' );
			// Explode data to fetch id and is_instant status
			$request_approve_array = explode('/', $request_approve_data);
			// Owner's Id
			$affiliate_vendor      = $request_approve_array[0];

			// Permissions that were changed by vendor
			$changed_permissions = empty($request->input('changed_permissions')) ? [] : $request->input('changed_permissions');

			$changed_permissions = json_decode($request->input('changed_permissions'), true);

			if ($changed_permissions) {
				$changed_permissions = array_flip($changed_permissions);
				$campaign_accesses   = array_intersect_key($campaign_accesses, $changed_permissions);
			} else {
				$campaign_accesses = [];
			}

			// As reply_to email we use first product's support email that isn't empty
            $reply_to_email = false;
            $reply_to_email_product_name = false;
            // As light_logo we use first product's light_logo that isn't empty
            $light_logo = false;

			// Array of edited products with their new status
			// We will send email to vendor with changed campaigns
			$changed_permissions = [];

			foreach ($campaign_accesses as $product_id => $access) {
				switch ($access) {
					case 'deny':
						AffiliateApproval::changeAffiliateAccess(Permissions::getOwnerByProduct($product_id)->owner_id, $affiliate_id, $product_id, AffiliateApproval::DENY);

						// Store product with status that was changed
						$product = Product::find($product_id);
						$changed_permissions[] = ['product_name' => $product->name, 'status' => $access];

						// Take first not empty support mail
                        if ( !empty($product->support_mail) && !$reply_to_email) {
                            $reply_to_email = $product->support_mail;
                            $reply_to_email_product_name = $product->name;
                        }

                        // Take first not empty light_logo
                        if ( !empty($product->light_logo) && !$light_logo) {
                            $light_logo = $product->light_logo;
                        }

						break;
					case 'delayed':
						AffiliateApproval::changeAffiliateAccess(Permissions::getOwnerByProduct($product_id)->owner_id, $affiliate_id, $product_id, AffiliateApproval::DELAYED);

						// Store product with status that was changed
						$product = Product::find($product_id);
						$changed_permissions[] = ['product_name' => $product->name, 'status' => $access];

                        // Take first not empty support mail
                        if ( !empty($product->support_mail) && !$reply_to_email) {
                            $reply_to_email = $product->support_mail;
                            $reply_to_email_product_name = $product->name;
                        }

                        // Take first not empty light_logo
                        if ( !empty($product->light_logo) && !$light_logo) {
                            $light_logo = $product->light_logo;
                        }

						break;
					case 'instant':
						AffiliateApproval::changeAffiliateAccess(Permissions::getOwnerByProduct($product_id)->owner_id, $affiliate_id, $product_id, AffiliateApproval::INSTANT);

						// Store product with status that was changed
						$product = Product::find($product_id);
						$changed_permissions[] = ['product_name' => $product->name, 'status' => $access];

                        // Take first not empty support mail
                        if ( !empty($product->support_mail) && !$reply_to_email) {
                            $reply_to_email = $product->support_mail;
                            $reply_to_email_product_name = $product->name;
                        }

                        // Take first not empty light_logo
                        if ( !empty($product->light_logo) && !$light_logo) {
                            $light_logo = $product->light_logo;
                        }

						break;
					default:
						break;
				}
			}

			// Set Affiliate's Auto-Approve access
			AffiliateApproval::changeAffiliateAutoApproveAccess($affiliate_id, $campaign_accesses, $auto_approve, $status, $affiliate_vendor);

			if (!empty($changed_permissions)) {
				event(new UpdateAffiliatePromoteEvent($affiliate_id, $affiliate_vendor, $changed_permissions, $reply_to_email, $reply_to_email_product_name, $light_logo));
			}

			\Session::flash( 'alert_message', 'The settings were saved!' );

			return redirect()->back();
		}

		/**
		 * Get # Sales and Refund Rate for affiliate
		 *
		 * @param $affiliate_id
		 *
		 * @return array
         */
        public static function getSalesAndRefund($affiliate_id)
		{
			$affiliate_commission_controller = new AffiliateCommissionsController();

            $data = $affiliate_commission_controller->processQueryData();

            $commissions_list = $affiliate_commission_controller->getCommissionsList($affiliate_id, $data, 1000000, true);

            $all_transactions = $commissions_list[ 'transactions' ]->paginate( 1000000 );

			// Paid, Unpaid, Refunded
			$approved_commissions = 0;
			// only Refunded
			$refunded_commissions = 0;

			foreach ($all_transactions as $transaction) {
				// All kind except of Denied
				if ($transaction->com_status != 3) {
					$approved_commissions++;
				}
				if ($transaction->is_refund) {
					$refunded_commissions++;
				}
			}

			if ($approved_commissions == 0) {
				return [
						'sales'       => 0,
						'refund_rate' => 0.0
				];
			}

			return [
				'sales'       => $approved_commissions,
			    'refund_rate' => round(($refunded_commissions / $approved_commissions) * 100, 1)
			];
		}
	}
