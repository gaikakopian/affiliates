/**
 * Created by Alex on 25/11/2016.
 */

/* @var CouponManager parent */
function ButtonManager( id, parent ) {
    var displayStates = {
        visible: 1,
        hidden : 2
    };
    var buttonStates = {
        submitted: 1,
        cancelled: 2,
        busy     : 3
    };
    var statuses = {
        neutral: 1,
        success: 2,
        fail   : 3
    };
    var self = this;
    var button = null, buttonParent = null, icon = null;
    var successClass = false;
    var errorClass = false;

    self.status = statuses.neutral;
    self.displayState = displayStates.hidden;
    self.buttonState = buttonStates.cancelled;

    self.getParent = function() {
        return buttonParent;
    };

    self.init = function() {
        button = $( id );

        button.on( "click", self.click.bind( self ) );
        icon = self.getIcon();

        buttonParent = button.parent( "div" );
        successClass = self.getSuccessClass();
        errorClass = self.getErrorClass();
    };

    self.checkDisplayState = function() {
        if( self.displayState === displayStates.visible ) {
            self.hide();
        } else {
            self.show();
        }
    };

    self.neutralStatus = function() {
        self.status = statuses.neutral;
        self.showSubmitIcon();
        self.buttonState = buttonStates.cancelled;
        self.remove();
    };

    self.successStatus = function() {
        self.status = statuses.success;
        self.showCancelIcon();
        self.buttonState = buttonStates.submitted;
        self.remove();
        buttonParent.addClass( successClass );
    };

    self.errorStatus = function() {
        self.status = statuses.error;
        self.showSubmitIcon();
        self.buttonState = buttonStates.cancelled;
        self.remove();
        buttonParent.addClass( errorClass );
    };

    self.show = function() {
        self.displayState = displayStates.visible;
        button.show();
    };

    self.hide = function() {
        self.displayState = displayStates.hidden;
        button.hide();
    };

    self.getIcon = function() {
        return button.find( 'i.fa' );
    };

    self.getUrl = function() {
        return button.data( "url" );
    };

    self.getSuccessClass = function() {
        return button.data( 'successClass' );
    };

    self.getErrorClass = function() {
        return button.data( 'errorClass' );
    };

    self.showSubmitIcon = function() {
        icon.removeClass( icon.data( "cancelIcon" ) );
        icon.removeClass( icon.data( "busyIcon" ) );
        icon.addClass( icon.data( "submitIcon" ) );
    };
    self.showCancelIcon = function() {
        icon.removeClass( icon.data( "submitIcon" ) );
        icon.removeClass( icon.data( "busyIcon" ) );
        icon.addClass( icon.data( "cancelIcon" ) );
    };

    self.showBusyIcon = function() {
        icon.removeClass( icon.data( "submitIcon" ) );
        icon.removeClass( icon.data( "cancelIcon" ) );
        icon.addClass( icon.data( "busyIcon" ) );
    };

    self.remove = function() {
        buttonParent.removeClass( errorClass );
        buttonParent.removeClass( successClass );
    };

    self.submit = function() {
        self.remove();
        if( parent && self.displayState == displayStates.visible ) {
            self.showBusyIcon();
            if( self.buttonState == buttonStates.cancelled ) {
                parent.submit();
            } else {
                parent.cancel();
            }
        }
    };

    self.click = function( evt ) {
        evt.preventDefault();
        self.submit();
    };

    self.init();
}

function CouponManager( element, parent ) {
    var displayStates = {
        visible: 1,
        hidden : 2
    };
    var inputStates = {
        empty : 1,
        filled: 2
    };
    var self = this, code = "", enabled = true, type = null, couponField = null, submitButtonParent = null;

    self.submitButton = null;
    self.hiddenField = null;
    self.displayState = displayStates.visible;
    self.inputState = inputStates.empty;

    checkState = function() {
        var prev_state = self.inputState;
        if( code.length > 0 ) {
            self.inputState = inputStates.filled;
        } else {
            self.inputState = inputStates.empty;
        }

        if( prev_state != self.inputState ) {
            self.submitButton.checkDisplayState();
        }
    };

    self.setCode = function( newCode ) {
        code = newCode;
        couponField.val( newCode );
        checkState();

        self.hiddenField.val( newCode );
    };

    self.getCode = function() {
        return code;
    };

    self.setType = function( newType ) {
        type = newType;
    };

    self.getType = function() {
        return type;
    };

    self.isEnabled = function( value ) {
        if( typeof value == "number" ) {
            enabled = value == 1;
        } else if( typeof value == "boolean" ) {
            enabled = value;
        } else {
            return enabled;
        }
        value ? self.show() : self.hide();
    };

    self.init = function() {
        self.hiddenField = $( "#coupon_code_hidden" );
        self.submitButton = new ButtonManager( ".template_checkout_coupon_apply", self );
        couponField = $( element );

        couponField.on( "keyup", self.keyup.bind( self ) );

        $( self ).on( "coupon:enable", self.enable );
        $( self ).on( "coupon:disable", self.disable );

        self.isEnabled( true );

        submitButtonParent = self.submitButton.getParent();
        self.setType( submitButtonParent.data( 'planType' ) );

        if( self.hiddenField.val() != "" ) {
            self.setCode( self.hiddenField.val() );
            self.submitButton.submit();
        } else {
            self.setCode( "" );
        }
    };

    self.enableInput = function() {
        couponField.removeAttr( "disabled" );
    };

    self.disableInput = function() {
        couponField.attr( "disabled", "disabled" );
    };

    self.submit = function() {
        apply( self.getCode() );
    };

    self.cancel = function() {
        apply( '$$clearcouponcode$$' );
    };

    self.show = function() {
        if( submitButtonParent && !submitButtonParent.is( ":visible" ) ) {
            submitButtonParent.fadeIn();
            self.displayState = displayStates.visible;
        }
    };

    self.hide = function() {
        if( submitButtonParent && !submitButtonParent.is( ":hidden" ) ) {
            submitButtonParent.fadeOut();
            self.displayState = displayStates.hidden;
        }
    };

    apply = function( code ) {
        var url = self.submitButton.getUrl();
        self.disableInput();
        $.ajax(
            {
                url     : url,
                dataType: 'json',
                type    : 'post',
                data    : {
                    funnel_id: $( "#hid_funnel" ).val(),
                    plan_id  : $( "#plan" ).val(),
                    code     : code
                },
                success : function( response ) {
                    if( response.success ) {
                        if( response.coupon_id ) {
                            self.submitButton.successStatus();
                        } else {
                            self.submitButton.neutralStatus();
                            self.enableInput();
                            self.setCode( '' );
                        }

                        $( CheckoutPageManager ).triggerHandler( "checkout:couponchanged" );
                    } else {
                        self.submitButton.errorStatus();
                        self.setCode( '' );
                        self.enableInput();
                    }
                },
                error   : function() {
                    // location.reload();
                }
            }
        ).done(
            function() {
            }
        );
    };

    self.keyup = function() {
        self.setCode( couponField.val() );
    };

    self.init();
};
